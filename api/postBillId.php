<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 4/1/2021
 * Time: 11:45 AM
 */

/* ====== setter ====== */
header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");
date_default_timezone_set("Asia/Bangkok");

/* ===== request model ===== */
require_once __DIR__.'/../model/BillModel.php';
$MBill = new BillModel();
require_once __DIR__.'/../model/BillListModel.php';
$MBList = new BillListModel();

/* ===== parameter ===== */
$res_return = [
    'code'=> 201,
    'message'=> 'not found',
    'result'=> []
];

/* ===== function ===== */
$billId = isset($_POST['id'])?$_POST['id']:'';
if($billId==''){
    $res_return['message'] = 'request parameter id !';
}
else{
    $res = $MBill->selectThisId($billId);
    if($res['status']){

        $bill = $res['result'];
        $list = [];

        $res = $MBList->selectByBillId($billId);
        if($res['status']){
            $list = $res['result'];
        }


        $res_return['code'] = 200;
        $res_return['message'] = 'success';
        $res_return['result']=[
            'bill'=> $bill,
            'list'=> $list
        ];

    }
    else{
        $res_return['message'] = $res['message'];
    }
}


/* ===== return ===== */
echo json_encode($res_return);

















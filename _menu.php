<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:45 AM
 */

$GROUPS = isset($GROUPS)?$GROUPS:[];
$S_SID  = isset($S_SID)?$S_SID:'';
$S_SName  = isset($S_SName)?$S_SName:'';
$S_SType  = isset($S_SType)?$S_SType:'';
$S_SCount = isset($S_SCount)?$S_SCount:0;
?>
<header>
    <div class="row flex-nowrap justify-content-between align-items-center bg-d">
        <div class="col text-center">
            <a class="a-none" href="#">
                <strong class="text-header t-d">
                    เปี๊ยะ ฟิน ฟิน
                </strong>
            </a>
        </div>

        <?php if($S_SCount>0):?>
        <div class="col-auto">
            <button type="button" class="btn btn-primary position-relative me-1 btn-sm" data-bs-toggle="modal" data-bs-target="#modalCartProduct">
                <i class="fas fa-shopping-cart"></i>
                <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                        +<?php echo $S_SCount;?>
                    </span>
            </button>
        </div>
        <?php endif;?>

    </div>
</header>

<div class="row bg-8">
    <div class="nav-scroller">
        <nav class="nav d-flex justify-content-center">
            <a class="p-2 text-white" href="page-index.php"><span style="padding-left: 60px;">หน้าหลัก</span></a>

            <?php foreach ($GROUPS as $item):?>
            <a class="p-2 text-white" href="page-search.php?search=<?php echo $item['group_name'];?>"><?php echo $item['group_name'];?></a>
            <?php endforeach;?>

            <!-- login -->
            <?php if($S_SID!=''):?>
                <a class="p-2 text-white" href="page-user.php"><i class="fas fa-user"></i> <?php echo $S_SName;?></a>
                <a class="p-2 text-white" href="_logout.php"><i class="fas fa-sign-out-alt"></i></a>
            <?php else:?>
                <a class="p-2 text-white" href="page-login.php">เข้าสู่ระบบ</a>
            <?php endif;?>
        </nav>
    </div>
</div>

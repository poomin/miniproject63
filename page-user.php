<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:42 AM
 */
require_once __DIR__.'/_session.php';
require_once __DIR__.'/controller/pageUserController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_header.php';?>

</head>
<body>


<!-- =========== Start: Top Menu =========== -->
<div class="container fixed-top">
    <?php require_once __DIR__.'/_menu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="container p-body">

    <div class="row">
        <div class="col-md-4 bg-white shadow rounded p-2">
            <h5 class="pb-2 border-bottom">Cherry</h5>
            <div class="list-group list-group-flush">
                <a href="?page=1" class="list-group-item d-flex justify-content-between align-items-center">
                    <span><i class="fas fa-shopping-cart"></i> ที่ต้องชำระ</span>
                    <?php if($S_SCount>0):?>
                    <span class="badge bg-danger rounded-pill"> +<?php echo $S_SCount;?> </span>
                    <?php endif;?>
                </a>
                <a href="?page=2" class="list-group-item d-flex justify-content-between align-items-center">
                    <span><i class="fas fa-truck"></i> ที่ดำเนินการ </span>
                    <?php if($count_send>0):?>
                    <span class="badge bg-danger rounded-pill"> <?php echo $count_send;?></span>
                    <?php endif;?>
                </a>

                <a href="?page=3" class="list-group-item list-group-item-action text-primary">
                    <i class="fas fa-download"></i> สำเร็จ
                </a>
                <a href="?page=4" class="list-group-item list-group-item-action text-primary">
                    <i class="fas fa-user"></i> ตั้งค่า
                </a>
            </div>
        </div>
        <div class="col-md-8">

            <!-- alert message -->
            <?php if($thisAlertStatus=='S'):?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <strong class="alert-heading">สำเร็จ</strong>
                    <span class="d-block"><?php echo $thisAlertText;?></span>
                </div>
            <?php elseif ($thisAlertStatus=='E'):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <strong class="alert-heading">แจ้งเตือน</strong>
                    <span class="d-block"><?php echo $thisAlertText;?></span>
                </div>
            <?php endif;?>


            <?php if($thisPage==1):?>

                <div class="bg-white shadow rounded p-2">
                    <h5 class="text-muted pb-2 border-bottom"><i class="fas fa-shopping-cart"></i> ที่ต้องชำระ</h5>


                    <?php if($S_SCount>0):?>
                    <div class="mt-2" style="min-height: 500px;">

                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <tbody>
                                <?php foreach ($VIEW_PAGE1 as $key=>$item):
                                    $sumPage1 += ($item['order_number']*$item['product_price']);
                                    ?>
                                    <tr class="p-2">
                                        <td><?php echo ($key+1);?></td>
                                        <td>
                                            <img class="img-fluid rounded" style="max-height: 60px;" src="<?php echo $item['img'];?>">
                                        </td>
                                        <td>
                                            <div>
                                                <strong class="fw-bold text-primary"><?php echo $item['product_name'];?></strong>
                                            </div>
                                            <div>
                                                ราคา <span class="badge bg-danger"><?php echo $item['product_price'];?></span> บาท
                                            </div>
                                            <div>
                                                <small class="text-muted"><?php echo $item['order_note'];?></small>
                                            </div>
                                            <div class="clearfix">
                                                <div class="float-start">
                                                    จำนวน <strong> <?php echo $item['order_number'];?> </strong>
                                                </div>
                                                <div class="float-end">
                                                    รวม <span class="badge bg-success"><?php echo number_format($item['order_number']*$item['product_price'],2,'.',',')?></span> บาท
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <button class="btn btn-outline-danger btn-sm" type="button" onclick="modalDeleteProductShow('<?php echo $item['id'];?>','<?php echo $item['product_name'];?>')">
                                                <i class="far fa-trash-alt"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>

                        <div class="text-end">
                            <span class="border-bottom border-info border-4 pb-1">
                                รวม <strong class="pe-2 ps-2 fw-bold alert-danger rounded"><?php echo number_format($sumPage1,2,'.',',');?></strong> บาท
                            </span>
                        </div>

                        <?php if($thisStep==1):?>
                            <div class="text-center">
                                <a class="btn btn-outline-success" href="?page=1&step=2">
                                     สั่งซื้อ <i class="fas fa-chevron-right"></i>
                                </a>
                            </div>
                        <?php elseif ($thisStep==2):?>

                        <form method="get">
                            <!-- send -->
                            <div class="p-2 mt-3">
                                <div class="mb-3">
                                    <h5>ค่าจัดส่ง</h5>
                                </div>
                                <div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="send" id="send1" value="30" checked>
                                        <label class="form-check-label" for="send1">
                                            จัดส่งแบบลงทะเบียน <span class="badge bg-danger">30</span> บาท
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="send" id="send2" value="50">
                                        <label class="form-check-label" for="send2">
                                            จัดส่งแบบ EMS <span class="badge bg-danger">50</span> บาท
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <!-- bank -->
                            <div class="p-2 mt-3">
                                <div class="mb-3">
                                    <h5>กรุณาเลือกแบงค์ชำระเงิน</h5>
                                </div>
                                <div class="row row-cols-1 row-cols-md-2 g-4">
                                    <?php foreach ($BANKS as $key=>$item):?>
                                        <div class="col">
                                            <div class="card mb-3 shadow">
                                                <div class="row g-0">
                                                    <div class="col-md-4 p-2">
                                                        <img class="img-fluid" src="/img/bank-<?php echo $item['bank_code'];?>.png" style="max-height: 100px;max-width: 100px;">
                                                    </div>
                                                    <div class="col-md-8 ps-3">
                                                        <div class="card-body">
                                                            <span class="d-block"><?php echo $item['bank_account'];?></span>
                                                            <strong class="d-block"><?php echo $item['bank_number'];?></strong>
                                                            <small class="d-block text-muted"><?php echo $item['bank_name'];?></small>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="bank" id="bank<?echo $item['id'];?>" value="<?php echo $item['id'];?>" <?php echo $key==0?'checked':'';?> >
                                                                <label class="form-check-label" for="bank<?echo $item['id'];?>">เลือก</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>

                            <div class="text-center mt-2 mb-3">

                                <div class="mb-3">
                                    <h5 class="pb-1">
                                        รวมยอดที่ต้องชำระ
                                        <strong id="p1s2" attr-sum="<?php echo $sumPage1;?>" class="pe-2 ps-2 fw-bold alert-danger rounded">
                                            <?php echo number_format(($sumPage1+30),2,'.',',');?>
                                        </strong>
                                        บาท
                                    </h5>
                                </div>

                                <input class="d-none" name="page" value="1" placeholder="">
                                <input class="d-none" name="step" value="3" placeholder="">
                                <a class="btn btn-outline-secondary" href="?page=1&step=1">
                                    <i class="fas fa-chevron-left"></i>
                                    <span class="ps-2 pe-2">กลับ</span>
                                </a>
                                <button class="btn btn-outline-success" type="submit">
                                    <span class="ps-2 pe-2">ถัดไป</span>
                                    <i class="fas fa-chevron-right"></i>
                                </button>
                            </div>

                        </form>

                        <?php elseif ($thisStep==3):?>

                            <form class="mt-2" method="post">
                                <div class="mb-1">
                                    <h5>ข้อมูลการรับสินค้า</h5>
                                </div>

                                <div class="row g-3 mb-3">
                                    <div class="col-md-6">
                                        <label class="t-r" for="inputNameId">ชื่อผู้รับสินค้า</label>
                                        <input id="inputNameId" class="form-control" name="member_name" value="<?php echo $memberName;?>">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="t-r" for="inputPhoneId">เบอร์โทร</label>
                                        <input id="inputPhoneId" class="form-control" name="member_tel" value="<?php echo $memberPhone;?>">
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="t-r" for="inputAddressId">ที่อยู่รับสินค้า</label>
                                    <textarea id="inputAddressId" class="form-control" name="member_address"><?php echo $memberAddress;?></textarea>
                                    <small class="form-text">กรุณากรอกข้อมูลให้ถูกต้องครบถ้วนเพื่อจัดส่งสินค้า</small>
                                </div>

                                <input class="d-none" type="text" name="bank" value="<?php echo $thisBankId;?>" placeholder="">
                                <input class="d-none" type="text" name="send" value="<?php echo $thisSend;?>" placeholder="">
                                <input id="slipId" class="d-none" type="text" name="slip" value="" placeholder="">
                                <input class="d-none" type="text" name="action" value="payment" placeholder="">
                                <button id="btnSendForm" class="btn d-none" type="submit">Send</button>
                            </form>

                            <!-- send -->
                            <div class="p-2 mt-3">
                                <div class="mb-1">
                                    <h5>จัดส่ง</h5>
                                </div>
                                <div>
                                    <?php if($thisSend==30):?>
                                        จัดส่งแบบลงทะเบียน <span class="badge bg-danger">30</span> บาท
                                    <?php elseif($thisSend==50):?>
                                        จัดส่งแบบ EMS <span class="badge bg-danger">50</span> บาท
                                    <?php endif;?>
                                </div>
                            </div>

                            <!-- bank -->
                            <div class="p-2 mt-3">
                                <div class="mb-3">
                                    <h5>แบงค์ชำระเงิน</h5>
                                </div>
                                <?php foreach ($BANKS as $key=>$item):
                                    if($item['id']== $thisBankId ):
                                    ?>
                                    <div class="card mb-3 shadow w-50">
                                        <div class="row g-0">
                                            <div class="col-auto p-2">
                                                <img class="img-fluid" src="/img/bank-<?php echo $item['bank_code'];?>.png" style="max-height: 80px;max-width: 80px;">
                                            </div>
                                            <div class="col ps-3">
                                                <div class="card-body">
                                                    <span class="d-block"><?php echo $item['bank_account'];?></span>
                                                    <strong class="d-block"><?php echo $item['bank_number'];?></strong>
                                                    <small class="d-block text-muted"><?php echo $item['bank_name'];?></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; endforeach;?>
                            </div>

                            <div class="mb-3 text-center">
                                <h5 class="pb-1">
                                    รวมยอดที่ต้องชำระ
                                    <strong class="pe-2 ps-2 fw-bold alert-danger rounded">
                                        <?php echo number_format(($sumPage1+30),2,'.',',');?>
                                    </strong>
                                    บาท
                                </h5>
                            </div>

                            <!-- upload image -->
                            <div class="mb-5">
                                <div class="mt-1">
                                    <div class="text-center mt-2">
                                        <label class="p-2 text-center" for="imageUploadId" style="cursor: pointer;">
                                            <img id="pathImageUploadId" class="img-fluid" src="img/df-transfer.jpg" style="max-height: 200px;border:2px dashed #0d6efd;">
                                            <input class="d-none" id="imageUploadId" accept="image/*" type="file" onchange="imageUpload(this)">
                                        </label>
                                    </div>
                                    <div class="text-center small text-muted">
                                        อัพโหลดภาพสลิปการชำระเงิน
                                    </div>
                                    <div class="row align-items-center d-none" id="imageUploadProgressbarId">
                                        <div class="col">
                                            <div class="progress">
                                                <div id="progressbarId" class="progress-bar" style="width: 1%;" role="progressbar"></div>
                                            </div>
                                        </div>
                                        <div class="col-auto pl-0">
                                <span id="cancelUploadImageId" class="badge bg-danger" style="cursor: pointer;" onclick="cancelUpload()">
                                    <i class="fas fa-times"></i>
                                </span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <!-- button -->
                            <div class="text-center mt-2 mb-3">

                                <a class="btn btn-outline-secondary" href="?page=1&step=2">
                                    <i class="fas fa-chevron-left"></i>
                                    <span class="ps-2 pe-2">กลับ</span>
                                </a>
                                <button class="btn btn-outline-success" type="button" onclick="fnSendPayment();">
                                    <span class="ps-2 pe-2">ยืนยันการชำระเงิน</span>
                                    <i class="fas fa-chevron-right"></i>
                                </button>
                            </div>

                        <?php endif;?>

                    </div>

                    <?php else:?>
                    <div class="mt-2" style="min-height: 500px;">
                        <div class="mt-5 text-center">
                            <h4 class="text-muted"> ไม่มีรายการในตระกร้า </h4>
                        </div>
                    </div>
                    <?php endif;?>


                </div>

            <?php elseif($thisPage==2):?>

                <div class="bg-white shadow rounded p-2">
                    <h5 class="text-muted pb-2 border-bottom"><i class="fas fa-truck"></i> ที่ดำเนินการ </h5>

                    <div class="mt-2" style="min-height: 500px;">
                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>วันที่</th>
                                    <th>OrderId</th>
                                    <th>จัดส่ง</th>
                                    <th>ยอดชำระ</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($BILLS as $key=>$item):?>
                                <tr>
                                    <td><?php echo ($key+1);?></td>
                                    <td><?php echo $item['bill_date'];?></td>
                                    <td><?php echo $item['id']?></td>
                                    <td>
                                        <div><?php echo $item['shipping_name'];?></div>
                                        <div class="d-block">
                                            ค่าจัดส่ง <span class="ps-2 pe-2 badge bg-danger"><?php echo $item['shipping_price'];?></span> บาท
                                        </div>
                                        <?php if(strlen($item['shipping_code'])> 5 ):?>
                                        <div>
                                            หมายเลขจัดส่ง <strong><?php echo $item['shipping_code'];?></strong>
                                        </div>
                                        <?php endif;?>

                                        <div class="text-center mt-2">
                                            <button class="btn btn-outline-info btn-sm w-50 p-0" type="button" onclick="modalViewShow(<?php echo $item['id'];?>)">รายละเอียด</button>
                                        </div>

                                    </td>
                                    <td><?php echo number_format(($item['bill_price']+$item['shipping_price']),2,'.',',');?></td>
                                    <td>
                                        <?php if($item['bill_type']== 'B'):?>
                                            <span class="badge bg-warning text-dark">รอตรวจสอบจากผู้ขาย</span>
                                        <?php elseif($item['bill_type']== 'P'):?>
                                            <span class="badge bg-info">รอจัดส่ง</span>
                                        <?php elseif($item['bill_type']== 'S'):?>
                                            <span class="badge bg-primary">รอรับสินค้า</span>
                                            <div class="mt-2">
                                                <button class="btn btn-outline-success btn-sm pt-0 pb-0" type="button" onclick="modalConfirmShow(<?php echo $item['id'];?>)"> ยืนยันได้รับสินค้า </button>
                                            </div>
                                        <?php else:?>
                                            <span class="badge bg-secondary"><?php echo $item['bill_type'];?></span>
                                        <?php endif;?>

                                    </td>
                                </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            <?php elseif($thisPage==3):?>

                <div class="bg-white shadow rounded p-2">
                    <h5 class="text-muted pb-2 border-bottom"><i class="fas fa-download"></i> สำเร็จ</h5>

                    <div class="mt-2" style="min-height: 500px;">
                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>วันที่</th>
                                    <th>OrderId</th>
                                    <th>จัดส่ง</th>
                                    <th>ยอดชำระ</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($FINISH as $key=>$item):?>
                                    <tr>
                                        <td><?php echo ($key+1);?></td>
                                        <td><?php echo $item['bill_date'];?></td>
                                        <td><?php echo $item['id']?></td>
                                        <td>
                                            <div><?php echo $item['shipping_name'];?></div>
                                            <div class="d-block">
                                                ค่าจัดส่ง <span class="ps-2 pe-2 badge bg-danger"><?php echo $item['shipping_price'];?></span> บาท
                                            </div>
                                            <?php if(strlen($item['shipping_code'])> 5 ):?>
                                                <div>
                                                    หมายเลขจัดส่ง <strong><?php echo $item['shipping_code'];?></strong>
                                                </div>
                                            <?php endif;?>
                                            <?php if(strlen($item['bill_note'])> 5 ):?>
                                                <div>
                                                    หมายเหตุ <strong><?php echo $item['bill_note'];?></strong>
                                                </div>
                                            <?php endif;?>

                                            <div class="text-center mt-2">
                                                <button class="btn btn-outline-info btn-sm w-50 p-0" type="button" onclick="modalViewShow(<?php echo $item['id'];?>)">รายละเอียด</button>
                                            </div>
                                        </td>
                                        <td><?php echo number_format(($item['bill_price']+$item['shipping_price']),2,'.',',');?></td>
                                        <td>
                                            <?php if($item['bill_type']== 'E'):?>
                                                <span class="badge bg-warning text-dark">สำเร็จ</span>
                                            <?php elseif($item['bill_type']== 'C'):?>
                                                <span class="badge bg-info">ยกเลิกรายการ</span>
                                            <?php else:?>
                                                <span class="badge bg-secondary"><?php echo $item['bill_type'];?></span>
                                            <?php endif;?>

                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            <?php else:?>

                <div class="bg-white shadow rounded p-2">
                    <h5 class="text-muted pb-2 border-bottom"><i class="fas fa-user"></i> ตั้งค่า</h5>

                    <div class="mt-2" style="min-height: 500px;">
                        content
                    </div>
                </div>

            <?php endif;?>

        </div>
    </div>


</div>
<!-- ____________ End: Body ____________ -->



<!-- =========== Start: Footer =========== -->
<div class="container-fluid footer pt-4 pb-4 bg-8">
    <?php require_once __DIR__.'/_footer.php';?>
</div>
<!-- ____________ End: Footer ____________ -->


<!-- =========== Start: Modal =========== -->
<!-- modal delete -->
<div class="modal fade" id="modalDelete" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content" method="post">
            <div class="modal-header">
                <h5 class="modal-title text-danger" id="modalDeleteLabel">ลบข้อมูล</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="text-center mb-3">
                    <i class="fas fa-ban fa-4x text-danger"></i>
                </div>
                <div class="text-center text-danger">
                    <strong>Delete</strong>
                    <p id="mDelText">ลบข้อมูล [สินค้า]</p>
                </div>
            </div>
            <div class="modal-footer">
                <input id="mDelId" class="d-none" type="text" name="id" value="" placeholder="">
                <input class="d-none" type="text" name="action" value="delProduct" placeholder="">

                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>
    </div>
</div>
<!-- modal Sell View -->
<div class="modal fade" id="modalView" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalViewLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalViewLabel">รายการสินค้า</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <div class="row mb-3">
                    <div class="col-md-8">
                        <div class="mb-3">

                            <div class="mb-1">
                                OrderId : <strong id="mVId" class="fw-bold text-muted me-5"></strong>
                                วันที่ : <strong id="mVDate" class="fw-bold text-muted me-5"></strong>
                            </div>
                            <div class="mb-1">
                                ลูกค้า: <strong id="mVName" class="fw-bold text-primary me-5"></strong>
                                เบอร์โทร: <strong id="mVTel" class="fw-bold text-primary"></strong>
                            </div>

                            <div class="mb-1">
                                ที่อยู่: <span id="mVAddress" class="fw-bold text-primary"></span>
                            </div>
                            <div class="mb-1">
                                วิธีการจัดส่ง <span id="mVShipName" class="fw-bold text-primary me-5"></span>
                                ราคาจัดส่ง <span id="mVShipPrice" class="fw-bold text-danger"></span> บาท
                            </div>
                            <div class="mb-1">
                                ราคาสินค้า <span id="mVPrice" class="fw-bold text-danger ms-2 me-1"></span> บาท
                            </div>
                            <div class="mb-3">
                                ยอดชำระ <span id="mVTotal" class="fw-bold text-danger ms-2 me-1"> </span> บาท
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <img id="mVImg" class="img-fluid img-thumbnail" src="img/df-upload.png" style="max-height: 150px;">
                        <span class="d-block text-muted">สลิป</span>
                    </div>
                </div>

                <div class="container">
                    <div class="table-responsive mb-5">
                        <table class="table table-bordered table-sm text-nowrap w-100">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>รายการ</th>
                                <th>เพิ่มเติม</th>
                                <th>จำนวน</th>
                                <th>ราคา</th>
                                <th>รวม</th>
                            </tr>
                            </thead>
                            <tbody id="mVTable"></tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal Sell Confirm -->
<div class="modal fade" id="modalConfirm" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalConfirmLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalConfirmLabel">ยืนยันการรับสินค้า</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <div class="row mb-3">
                    <div class="col-md-8">
                        <div class="mb-3">

                            <div class="mb-1">
                                OrderId : <strong id="mCId" class="fw-bold text-muted me-5"></strong>
                                วันที่ : <strong id="mCDate" class="fw-bold text-muted me-5"></strong>
                            </div>
                            <div class="mb-1">
                                ลูกค้า: <strong id="mCName" class="fw-bold text-primary me-5"></strong>
                                เบอร์โทร: <strong id="mCTel" class="fw-bold text-primary"></strong>
                            </div>

                            <div class="mb-1">
                                ที่อยู่: <span id="mCAddress" class="fw-bold text-primary"></span>
                            </div>
                            <div class="mb-1">
                                วิธีการจัดส่ง <span id="mCShipName" class="fw-bold text-primary me-5"></span>
                                ราคาจัดส่ง <span id="mCShipPrice" class="fw-bold text-danger"></span> บาท
                            </div>
                            <div class="mb-1">
                                ราคาสินค้า <span id="mCPrice" class="fw-bold text-danger ms-2 me-1"></span> บาท
                            </div>
                            <div class="mb-3">
                                ยอดชำระ <span id="mCTotal" class="fw-bold text-danger ms-2 me-1"> </span> บาท
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <img id="mCImg" class="img-fluid img-thumbnail" src="img/df-upload.png" style="max-height: 150px;">
                        <span class="d-block text-muted">สลิป</span>
                    </div>
                </div>

                <div class="container">
                    <div class="table-responsive mb-5">
                        <table class="table table-bordered table-sm text-nowrap w-100">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>รายการ</th>
                                <th>เพิ่มเติม</th>
                                <th>จำนวน</th>
                                <th>ราคา</th>
                                <th>รวม</th>
                            </tr>
                            </thead>
                            <tbody id="mCTable"></tbody>
                        </table>
                    </div>
                </div>

            </div>
            <form class="modal-footer" method="post">
                <input class="d-none" type="text" name="action" value="confirm" placeholder="">
                <input id="mCBillId" class="d-none" type="text" name="bill_id" value="" placeholder="">

                <button type="submit" class="btn btn-success">ยืนยันได้รับสินค้า</button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </form>
        </div>
    </div>
</div>

<!-- ____________ End: Modal ____________ -->


<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_script.php';?>
<!-- ____________ End: Script ____________ -->

<!-- modal product shop -->
<?php require_once __DIR__.'/_modalProduct.php';?>


<!-- =========== Start: Custom Script =========== -->
<script>
    function formatNumber(num) {
        return num.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    }

    //delete product
    function modalDeleteProductShow(id,text) {
        $("#modalDelete").modal("show");

        $("#mDelText").html("ลบรายการในตะกร้า "+text);
        $("#mDelId").val(id);
    }


    //change send payment
    $('input:radio[name="send"]').on('click change', function(e) {
        let val = $(this).val();
        let sum = $("#p1s2").attr("attr-sum");
        val = parseFloat(val);
        sum = parseFloat(sum);
        sum = sum+val;
        $("#p1s2").html(formatNumber(sum));

    });


    //upload image
    let ajax_img;
    function imageUpload(input) {
        if (input.files && input.files[0]) {
            ajax_img = new XMLHttpRequest();

            $('#imageUploadProgressbarId').removeClass('d-none');

            let form_data = new FormData();
            form_data.append("fileToUpload", input.files[0]);
            ajax_img.upload.addEventListener("progress", progressHandler, false);
            ajax_img.addEventListener("load", completeHandler, false);
            ajax_img.addEventListener("error", errorHandler, false);
            ajax_img.addEventListener("abort", abortHandler, false);
            ajax_img.open("POST","/upload/upload_file.php");
            ajax_img.send(form_data);

            function progressHandler(event) {
                let percent = (event.loaded / event.total) * 100;
                $("#progressbarId").css('width', Math.round(percent) + "%");
            }

            function completeHandler(event) {
                let data_return = JSON.parse(event.target.responseText);
                if (data_return['status'] === 'ok') {
                    let src = '/upload/uploads/'+data_return['new_name'];

                    $("#progressbarId").css('width', "0%");
                    $("#imageUploadProgressbarId").addClass("d-none");

                    $("#pathImageUploadId").attr("src",src);

                    $("#slipId").val(src);

                } else {
                    ajax_img.abort();
                    $("#progressbarId").css('width', "0%");
                    $("#imageUploadProgressbarId").addClass("d-none");
                    alert("Error:" + data_return['message']);
                }
            }

            function errorHandler(event) {
                ajax_img.abort();
                $("#progressbarId").css('width', "0%");
                $("#imageUploadProgressbarId").addClass("d-none");
                alert("Upload Failed");
            }

            function abortHandler(event) {
                ajax_img.abort();
                $("#progressbarId").css('width', "0%");
                $("#imageUploadProgressbarId").addClass("d-none");
                alert("Upload Aborted");
            }
        }
        else {
            alert("Not found file input!!!");
        }
    }
    function cancelUpload() {
        if(ajax_img!=null){
            ajax_img.abort();
        }
        $("#progressbarId").css('width', "0%");
        $("#imageUploadProgressbarId").addClass("d-none");
        alert("Cancel Upload Image");
    }

    //send payment
    function fnSendPayment() {
        let name = $("#inputNameId").val();
        let phone = $("#inputPhoneId").val();
        let address = $("#inputAddressId").val();
        let slip = $("#slipId").val();

        if(name===undefined || name===""){
            alert("please input name");
        }
        else if(phone===undefined || phone===""){
            alert("please input phone");
        }
        else if(address===undefined || address===""){
            alert("please input address");
        }
        else if(slip===undefined || slip===""){
            alert("please upload slip");
        }
        else{
            $("#btnSendForm").click();
        }

    }


    //view
    function modalViewShow(id) {
        let req = $.ajax({
            type: 'POST',
            url: '/api/postBillId.php',
            data: {
                id: id,
            },
            dataType: 'JSON'
        });
        req.done(function (res) {
            if(res.code === 200 ){
                //success
                console.log(res);
                let data = res.result;
                let bill = data.bill;
                let list = data.list;

                let shipPrice = bill.shipping_price;
                let proPrice = bill.bill_price;
                let sum = shipPrice + proPrice;

                //$("#mVBillId").val(bill.id);
                $("#mVId").html(bill.id);
                $("#mVDate").html(bill.bill_date);

                $("#mVName").html(bill.member_name);
                $("#mVTel").html(bill.member_tel);
                $("#mVAddress").html(bill.member_address);

                $("#mVShipName").html(bill.shipping_name);
                $("#mVShipPrice").html(shipPrice);

                $("#mVImg").attr("src",bill.slip_img);

                $("#mVPrice").html(proPrice);
                $("#mVTotal").html(sum);

                let textTable = "";
                let str = "";
                for(let i=0;i<list.length;i++){
                    str = "<tr>";
                    str += "<td>"+(i+1)+"</td>";
                    str += "<td>"+list[i].product_name+"</td>";
                    str += "<td>"+list[i].order_note+"</td>";
                    str += "<td>"+list[i].order_number+"</td>";
                    str += "<td>"+list[i].product_price+"</td>";
                    str += "<td>"+(list[i].order_number*list[i].product_price)+"</td>";
                    str += "</tr>";
                    textTable+=str;
                }
                $("#mVTable").html(textTable);


                $("#modalView").modal("show");
            }else{
                //warning
                alert("Warn : "+res.message);
            }
        }).fail(function (e,text){
            //in page show error
            alert("Error : "+text)
        });
    }
    //confirm
    function modalConfirmShow(id) {
        let req = $.ajax({
            type: 'POST',
            url: '/api/postBillId.php',
            data: {
                id: id,
            },
            dataType: 'JSON'
        });
        req.done(function (res) {
            if(res.code === 200 ){
                //success
                console.log(res);
                let data = res.result;
                let bill = data.bill;
                let list = data.list;

                let shipPrice = bill.shipping_price;
                let proPrice = bill.bill_price;
                let sum = shipPrice + proPrice;

                $("#mCBillId").val(bill.id);
                $("#mCId").html(bill.id);
                $("#mCDate").html(bill.bill_date);

                $("#mCName").html(bill.member_name);
                $("#mCTel").html(bill.member_tel);
                $("#mCAddress").html(bill.member_address);

                $("#mCShipName").html(bill.shipping_name);
                $("#mCShipPrice").html(shipPrice);

                $("#mCImg").attr("src",bill.slip_img);

                $("#mCPrice").html(proPrice);
                $("#mCTotal").html(sum);

                let textTable = "";
                let str = "";
                for(let i=0;i<list.length;i++){
                    str = "<tr>";
                    str += "<td>"+(i+1)+"</td>";
                    str += "<td>"+list[i].product_name+"</td>";
                    str += "<td>"+list[i].order_note+"</td>";
                    str += "<td>"+list[i].order_number+"</td>";
                    str += "<td>"+list[i].product_price+"</td>";
                    str += "<td>"+(list[i].order_number*list[i].product_price)+"</td>";
                    str += "</tr>";
                    textTable+=str;
                }
                $("#mCTable").html(textTable);


                $("#modalConfirm").modal("show");
            }else{
                //warning
                alert("Warn : "+res.message);
            }
        }).fail(function (e,text){
            //in page show error
            alert("Error : "+text)
        });
    }


</script>
<!-- ____________ End: Custom Script ____________ -->


</body>
</html>

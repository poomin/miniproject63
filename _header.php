<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:44 AM
 */
?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="img/logo-page.ico">

<title>Cherry Shop</title>

<!-- bootstrap css -->
<link rel="stylesheet" href="bootstrap-5.0.0/css/bootstrap.min.css">

<!-- font awesome -->
<link rel="stylesheet" href="fontawesome-5.15.1/css/all.min.css">

<!-- custom css -->
<link rel="stylesheet" href="css/custom-css.css">

<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:47 AM
 */
?>
<div class="row">
    <div class="col text-center text-white">
        <div class="p-1">
            <h4><?php echo isset($SHOP['shop_name'])?$SHOP['shop_name']:'';?></h4>
        </div>
        <span class="p-1">
            <i class="fa fa-phone text-info"></i>
            <?php echo isset($SHOP['tel'])?$SHOP['tel']:'';?>
        </span>
        <span class="p-1">
            <i class="fab fa-line text-success"></i>
            <a class="text-white" href="<?php echo isset($SHOP['line_link'])?$SHOP['line_link']:'#';?>">
                <?php echo isset($SHOP['line_name'])?$SHOP['line_name']:'';?>
            </a>
        </span>
        <span class="p-1">
            <i class="fab fa-facebook-f text-primary"></i>
            <a class="text-white" href="<?php echo isset($SHOP['fb_link'])?$SHOP['fb_link']:'#';?>" target="_blank">
                <?php echo isset($SHOP['fb_name'])?$SHOP['fb_name']:'';?>
            </a>
        </span>
        <div class="p-1">
            <i class="fas fa-map-marker text-danger"></i> ที่อยู่ร้าน
            <span class="ms-2">
                <?php echo isset($SHOP['address'])?$SHOP['address']:'';?>
            </span>
        </div>
    </div>
</div>

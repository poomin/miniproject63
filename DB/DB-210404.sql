-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.17-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table db_mini_project_shop.bank
CREATE TABLE IF NOT EXISTS `bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_code` varchar(50) DEFAULT NULL,
  `bank_number` varchar(50) DEFAULT NULL,
  `bank_account` varchar(50) DEFAULT NULL,
  `this_remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table db_mini_project_shop.bank: ~2 rows (approximately)
DELETE FROM `bank`;
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
INSERT INTO `bank` (`id`, `bank_code`, `bank_number`, `bank_account`, `this_remove`) VALUES
	(1, 'kkb', '045-xxxxx-xx', 'รัตติ สิริน', 'N');
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;

-- Dumping structure for table db_mini_project_shop.bill
CREATE TABLE IF NOT EXISTS `bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `member_name` varchar(50) DEFAULT NULL,
  `member_address` varchar(255) DEFAULT NULL,
  `member_tel` varchar(50) DEFAULT NULL,
  `bill_type` enum('O','B','P','S','E','C') DEFAULT 'B',
  `bill_price` double DEFAULT '0',
  `bill_date` date DEFAULT NULL,
  `bill_note` varchar(255) DEFAULT NULL,
  `shipping_name` varchar(50) DEFAULT NULL,
  `shipping_price` double DEFAULT '0',
  `shipping_code` varchar(50) DEFAULT NULL,
  `bank_code` varchar(50) DEFAULT NULL,
  `bank_number` varchar(50) DEFAULT NULL,
  `slip_img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_mini_project_shop.bill: ~0 rows (approximately)
DELETE FROM `bill`;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;

-- Dumping structure for table db_mini_project_shop.bill_list
CREATE TABLE IF NOT EXISTS `bill_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) DEFAULT '0',
  `member_id` int(11) DEFAULT '0',
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `product_price` double DEFAULT NULL,
  `order_number` int(11) DEFAULT NULL,
  `order_note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_mini_project_shop.bill_list: ~0 rows (approximately)
DELETE FROM `bill_list`;
/*!40000 ALTER TABLE `bill_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_list` ENABLE KEYS */;

-- Dumping structure for table db_mini_project_shop.member
CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `member_name` varchar(50) DEFAULT NULL,
  `member_type` enum('A','U') DEFAULT 'U',
  `address` varchar(255) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `this_remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table db_mini_project_shop.member: ~0 rows (approximately)
DELETE FROM `member`;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` (`id`, `username`, `password`, `member_name`, `member_type`, `address`, `tel`, `this_remove`) VALUES
	(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin cherry', 'A', 'เลขที่ 78  หมู่ 4 ถนนวาริน-เดช ต.ธาตุ อ.วารินชำราบ จ.อุบลราชธานี 34190', '08-xxxx-xxxx', 'N'),
	(2, 'user', '81dc9bdb52d04dc20036dbd8313ed055', 'คุณบุญศรี ใจดี', 'U', 'เลขที่ 78  หมู่ 4 ถนนวาริน-เดช ต.ธาตุ อ.วารินชำราบ จ.อุบลราชธานี 34190', '08-xxxx-xxxx', 'N');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;

-- Dumping structure for table db_mini_project_shop.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `price` double DEFAULT '0',
  `img` varchar(255) DEFAULT NULL,
  `detail` text,
  `this_remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table db_mini_project_shop.product: ~4 rows (approximately)
DELETE FROM `product`;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `group_id`, `product_name`, `price`, `img`, `detail`, `this_remove`) VALUES
	(1, 1, 'เปี๊ยะลาวาไข่เค็ม 2 ชิ้น', 50, '/upload/uploads/up_1615285137.jpg', 'อิ่มพอดีคำกับ เปี๊ยะลาวาไข่เค็ม 2 ชิ้น\r\n- ใส่ไข่เค็ม\r\n- จำนวน 2 ชิ้น / กล่อง', 'N'),
	(2, 1, 'เปี๊ยะลาวาไข่เค็ม 4 ชิ้น', 90, '/upload/uploads/up_1615286447.jpg', 'อิ่มพอดีคำกับ เปี๊ยะลาวาไข่เค็ม 4 ชิ้น', 'N'),
	(3, 1, 'เปี๊ยะลาวาไข่เค็ม 6 ชิ้น', 130, '/upload/uploads/up_1615287138.jpg', 'ฟินจัดเต็มด้วยไข่เค็มลาวาแน่นๆ 6 ชิ้นไปเลยจร้า', 'N'),
	(4, 2, 'เปี๊ยะพาสเทลไส้ถั่วไข่เค็ม 6 ชิ้น', 100, '/upload/uploads/up_1615287220.jpg', '', 'N'),
	(5, 2, 'เปี๊ยะพาสเทลไส้ถั่ว 6 ชิ้น', 50, '/upload/uploads/up_1615287315.jpg', '', 'N');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table db_mini_project_shop.product_comment
CREATE TABLE IF NOT EXISTS `product_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `comment_name` varchar(50) DEFAULT NULL,
  `comment_post` varchar(255) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_mini_project_shop.product_comment: ~0 rows (approximately)
DELETE FROM `product_comment`;
/*!40000 ALTER TABLE `product_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_comment` ENABLE KEYS */;

-- Dumping structure for table db_mini_project_shop.product_group
CREATE TABLE IF NOT EXISTS `product_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) DEFAULT NULL,
  `this_remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table db_mini_project_shop.product_group: ~3 rows (approximately)
DELETE FROM `product_group`;
/*!40000 ALTER TABLE `product_group` DISABLE KEYS */;
INSERT INTO `product_group` (`id`, `group_name`, `this_remove`) VALUES
	(1, 'เปี๊ยะลาวาไข่เค็ม', 'N'),
	(2, 'เปี๊ยะพาสเทล', 'N'),
	(3, 'เปี๊ยะใส่ไส้', 'Y');
/*!40000 ALTER TABLE `product_group` ENABLE KEYS */;

-- Dumping structure for table db_mini_project_shop.product_upload
CREATE TABLE IF NOT EXISTS `product_upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_mini_project_shop.product_upload: ~0 rows (approximately)
DELETE FROM `product_upload`;
/*!40000 ALTER TABLE `product_upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_upload` ENABLE KEYS */;

-- Dumping structure for table db_mini_project_shop.shop
CREATE TABLE IF NOT EXISTS `shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(50) DEFAULT NULL,
  `shop_owner` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `google_map` text,
  `tel` varchar(255) DEFAULT NULL,
  `fb_name` varchar(50) DEFAULT NULL,
  `fb_link` varchar(255) DEFAULT NULL,
  `line_name` varchar(50) DEFAULT NULL,
  `line_link` varchar(255) DEFAULT NULL,
  `line_qrcode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table db_mini_project_shop.shop: ~0 rows (approximately)
DELETE FROM `shop`;
/*!40000 ALTER TABLE `shop` DISABLE KEYS */;
INSERT INTO `shop` (`id`, `shop_name`, `shop_owner`, `address`, `google_map`, `tel`, `fb_name`, `fb_link`, `line_name`, `line_link`, `line_qrcode`) VALUES
	(1, 'เปี๊ยะ ฟิน ฟิน', 'รัตติ สิริน', 'เลขที่ 78  หมู่ 4 ถนนวาริน-เดช ต.ธาตุ อ.วารินชำราบ จ.อุบลราชธานี 34190', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3851.672959699736!2d104.8960392152586!3d15.121342668155599!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311684e6ca50733f%3A0xffd330ebb73eacca!2z4Lir4Lit4Lie4Lix4LiB4Lir4LiN4Li04LiH4LiB4LiK4Lie4Lij!5e0!3m2!1sth!2sth!4v1615276776157!5m2!1sth!2sth', '08-xxxx-xxxx', 'PooMin JaiChoung', 'https://www.facebook.com/poomin.jaichoung/', 'Poomin', 'https://line.me/ti/p/3cQQQ0t0Qr', '/upload/uploads/up_1615278851.png');
/*!40000 ALTER TABLE `shop` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

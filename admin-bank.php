<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 12:31 PM
 */
$MENU = 'bank';
require_once __DIR__.'/controller/adminBankController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_adminHeader.php';?>

    <style>
        .t-r:after{
            content: "*";
            color: red;
        }
    </style>

</head>
<body>

<!-- =========== Start: Top Menu =========== -->
<div class="sticky-top">
    <?php require_once __DIR__.'/_adminMenu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="pl-2 pr-2">
    <div class="container-fluid">
        <div class="page-content shadow mt-3 mb-5 p-3">

            <!-- header -->
            <div class="mb-3 border-bottom clearfix pb-1">
                <div class="float-start">
                    <h5 class="float-left"><i class="fas fa-university"></i> ธนาคาร</h5>
                </div>
                <div class="float-end">
                    <button class="btn btn-outline-success btn-sm" type="button" onclick="fnNewModal()">
                        <i class="fas fa-plus-square"></i>
                    </button>
                </div>
            </div>

            <!-- alert message -->
            <?php if($thisAlertStatus=='S'):?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <strong class="alert-heading">สำเร็จ</strong>
                    <span class="d-block"><?php echo $thisAlertText;?></span>
                </div>
            <?php elseif ($thisAlertStatus=='E'):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <strong class="alert-heading">แจ้งเตือน</strong>
                    <span class="d-block"><?php echo $thisAlertText;?></span>
                </div>
            <?php endif;?>

            <!-- table list -->
            <div class="container">
                <div class="table-responsive mb-5">
                    <table class="dataTableNone table table-sm text-nowrap w-100">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ธนาคาร</th>
                            <th>เลขบัญชี</th>
                            <th>เจ้าของบัญชี</th>
                            <th>จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($BANKS as $key=>$item):?>
                        <tr>
                            <td><?php echo ($key+1);?></td>
                            <td><?php echo $item['bank_name'];?></td>
                            <td><?php echo $item['bank_number'];?></td>
                            <td><?php echo $item['bank_account'];?></td>
                            <td>
                                <button class="btn btn-outline-warning btn-sm" type="button" onclick="fnEditModal('<?php echo $item['id'];?>','<?php echo $item['bank_code'];?>','<?php echo $item['bank_number'];?>','<?php echo $item['bank_account'];?>')">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button type="button" class="btn btn-outline-danger btn-sm" onclick="fnDeleteModal('<?php echo $item['id'];?>','<?php echo $item['bank_account'];?>')">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

</div>
<!-- ____________ End: Body ____________ -->

<!-- =========== Start: Modal =========== -->
<!-- modal Delete -->
<div class="modal fade" id="modalDelete" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-danger" id="modalDeleteLabel">ลบข้อมูล</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="text-center mb-3">
                    <i class="fas fa-ban fa-4x text-danger"></i>
                </div>
                <div class="text-center text-danger">
                    <strong>Delete</strong>
                    <p id="mDText">ลบข้อมูล</p>
                </div>
            </div>
            <form class="modal-footer" method="post">
                <input class="d-none" type="text" name="action" value="delete" placeholder="">
                <input id="mDId" class="d-none" type="text" name="bank_id" value="" placeholder="">

                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </div>
    </div>
</div>
<!-- modal Bank -->
<div class="modal fade" id="modalBank" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalBankLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content" method="post">
            <div class="modal-header">
                <h5 class="modal-title" id="modalBankLabel">ธนาคาร</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="mb-3">
                    <label class="t-r" for="mBankCode">ธนาคาร </label>
                    <select id="mBankCode" class="form-select" name="bank_code" required>
                        <option value="">--เลือก--</option>
                        <?php foreach ($bankCode as $key=>$item):?>
                        <option value="<?php echo $key;?>"><?php echo $item;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="t-r" for="mBankNumber">หมายเลขบัญชี </label>
                    <input id="mBankNumber" class="form-control" type="text" name="bank_number" required>
                </div>

                <div class="mb-3">
                    <label class="t-r" for="mBankAcc">ชื่อเจ้าของบัญชี </label>
                    <input id="mBankAcc" class="form-control" type="text" name="bank_account" required>
                </div>
            </div>
            <div class="modal-footer">
                <input id="mBankAction" class="d-none" type="text" name="action" value="insert" placeholder="">
                <input id="mBankId" class="d-none" type="text" name="bank_id" value="" placeholder="">

                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button id="mBankBtn" type="submit" class="btn btn-success">Edit</button>
            </div>
        </form>
    </div>
</div>
<!-- ____________ End: Modal ____________ -->

<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_adminScript.php';?>
<!-- ____________ End: Script ____________ -->


<!-- =========== Start: Custom Script =========== -->
<script>
    //new
    function fnNewModal() {
        $("#mBankCode").val("");
        $("#mBankNumber").val("");
        $("#mBankAcc").val("");
        $("#mBankAction").val("insert");
        $("#mBankBtn").html("บันทึก");

        $("#modalBank").modal("show");
    }
    //edit
    function fnEditModal(bId,bCode,bNum,bAcc) {
        $("#mBankCode").val(bCode);
        $("#mBankNumber").val(bNum);
        $("#mBankAcc").val(bAcc);
        $("#mBankAction").val("update");
        $("#mBankBtn").html("แก้ไข");
        $("#mBankId").val(bId);

        $("#modalBank").modal("show");
    }
    //delete
    function fnDeleteModal(bId,bAcc) {
        $("#mDText").html("ลบข้อมูล "+bAcc);
        $("#mDId").val(bId);

        $("#modalDelete").modal("show");
    }

</script>

<!-- ____________ End: Custom Script ____________ -->


</body>

</html>

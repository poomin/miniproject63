<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/18/2021
 * Time: 9:56 AM
 */
require_once __DIR__.'/_session.php';
require_once __DIR__.'/controller/pageSearchController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_header.php';?>

</head>
<body>


<!-- =========== Start: Top Menu =========== -->
<div class="container fixed-top">
    <?php require_once __DIR__.'/_menu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="container p-body">

    <!-- slide image -->
    <div id="carouselImg" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">

            <?php foreach ($PRODUCTS as $key=>$item):?>
            <div class="carousel-item <?php echo $key==0?'active':'';?>">
                <img src="<?php echo $item['img'];?>" class="d-block w-100">
                <div class="carousel-caption bg-8 rounded">
                    <span class="d-block"><?php echo $item['product_name'];?></span>
                    <span class="d-block">
                        <i class="fa fa-tag t-d"></i>
                        <strong class="px-2"><?php echo $item['price'];?></strong> บาท
                    </span>
                </div>
            </div>
            <?php endforeach;?>

        </div>
        <button class="carouse-pre" type="button" data-bs-target="#carouselImg"  data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        </button>
        <button class="carouse-next" type="button" data-bs-target="#carouselImg"  data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
        </button>
    </div>

    <!-- list order Search -->
    <div class="shadow p-2 mt-5 bg-white bg-doc">
        <div class="p-2">
            <div class="pb-2 border-bottom">
                <form class="input-group" style="max-width: 500px;">
                    <input type="text" class="form-control" name="search" placeholder="ค้นหา" aria-label="ค้นหา" aria-describedby="btnSearch">
                    <button class="btn btn-outline-secondary" type="submit" id="btnSearch"><i class="fas fa-search"></i></button>
                </form>
            </div>
        </div>
        <div class="p-2">
            <div class="mb-3">
                <h5> <i class="fas fa-search text-muted"></i> รายการค้นหา</h5>
            </div>
            <div class="container-fluid">
                <div class="row row-cols-2 row-cols-md-4 g-3">
                    <?php foreach ($SEARCH as $item):?>
                    <div class="col card-box-news p-1" style="cursor: pointer;" onclick="window.location.href='page-view.php?id=<?php echo $item['id'];?>';">
                        <div class="card shadow bg-cake">
                            <img src="<?php echo $item['img'];?>" class="card-img-top card-gallery-img">
                            <div class="card-body p-1">
                                <small class="card-title text-center"><?php echo $item['product_name'];?></small>
                                <div class="clearfix">
                                    <div class="float-end">
                                        <span class="small badge bg-danger"><?php echo $item['price'];?> -</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>

    <!-- slide production-->
    <div class="shadow p-2 mt-5 bg-white">
        <div class="p-2">
            <h5 class="text-secondary pb-2 border-bottom"><i class="fas fa-cookie-bite mx-2"></i> เปี๊ยะ ฟิน ฟิน จร้า ^^</h5>
        </div>
        <div class="table-responsive">
            <table class="table table-borderless text-nowrap">
                <tr>
                    <?php foreach ($PRODUCTS as $key=>$item):?>
                        <td>
                            <div class="hover-shadow card shadow bg-cake" style="max-width: 200px;" onclick="window.location.href='page-view.php?id=<?php echo $item['id'];?>';">
                                <img src="<?php echo $item['img'];?>" class="card-img-top card-gallery-img">
                                <div class="card-body p-1">
                                    <small class="card-title text-center"><?php echo $item['product_name'];?></small>
                                    <div class="clearfix">
                                        <div class="float-end">
                                            <span class="small badge bg-danger"><?php echo $item['price'];?> -</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    <?php endforeach;?>
                </tr>
            </table>
        </div>
    </div>

</div>
<!-- ____________ End: Body ____________ -->


<!-- =========== Start: Footer =========== -->
<div class="container-fluid footer pt-4 pb-4 bg-8">
    <?php require_once __DIR__.'/_footer.php';?>
</div>
<!-- ____________ End: Footer ____________ -->


<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_script.php';?>
<!-- ____________ End: Script ____________ -->

<!-- modal product shop -->
<?php require_once __DIR__.'/_modalProduct.php';?>


<!-- =========== Start: Custom Script =========== -->
<script>


</script>
<!-- ____________ End: Custom Script ____________ -->


</body>
</html>

<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 12:31 PM
 */
$MENU = 'product';
require_once __DIR__.'/controller/adminProductActionController.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_adminHeader.php';?>

</head>
<body>

<!-- =========== Start: Top Menu =========== -->
<div class="sticky-top">
    <?php require_once __DIR__.'/_adminMenu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="pl-2 pr-2">
    <div class="container-fluid">
        <div class="page-content shadow mt-3 mb-5 p-3">

            <!-- header -->
            <div class="mb-3 border-bottom pb-1">
                <a class="text-decoration-none me-2" href="admin-product.php">
                    <span class="h5"><i class="fas fa-gifts"></i> สินค้า</span>
                </a>  /
                <span class="text-muted h5 ms-2"> เพิ่ม,แก้ไข</span>
            </div>

            <!-- alert message -->
            <?php if($thisAlertStatus=='S'):?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <strong class="alert-heading">สำเร็จ</strong>
                    <span class="d-block"><?php echo $thisAlertText;?></span>
                </div>
            <?php elseif ($thisAlertStatus=='E'):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <strong class="alert-heading">แจ้งเตือน</strong>
                    <span class="d-block"><?php echo $thisAlertText;?></span>
                </div>
            <?php endif;?>

            <!-- form input -->
            <div class="container mb-3">

                <!-- upload image -->
                <div class="mb-5">
                    <div class="mt-1">
                        <div class="text-center mt-2">
                            <label class="p-2 text-center" for="imageUploadId" style="cursor: pointer;">
                                <img id="pathImageUploadId" class="img-fluid" src="<?php echo $thisProductImg;?>" style="max-height: 300px;max-width: 350px;border:2px dashed #0d6efd;">
                                <input class="d-none" id="imageUploadId" accept="image/*" type="file" onchange="imageUpload(this)">
                            </label>
                        </div>
                        <div class="text-center small text-muted">
                            อัพโหลดภาพหน้าปก ( 400 x 1000 px )
                        </div>
                        <div class="row align-items-center d-none" id="imageUploadProgressbarId">
                            <div class="col">
                                <div class="progress">
                                    <div id="progressbarId" class="progress-bar" style="width: 1%;" role="progressbar"></div>
                                </div>
                            </div>
                            <div class="col-auto pl-0">
                                <span id="cancelUploadImageId" class="badge bg-danger" style="cursor: pointer;" onclick="cancelUpload()">
                                    <i class="fas fa-times"></i>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- form product -->
                <form method="post">

                    <div class="mb-3">
                        <label for="inputProductGroup">กลุ่ม/ประเภท สินค้า</label>
                        <div class="input-group mb-3">
                            <select class="form-select" id="inputProductGroup" name="group_id" required>
                                <option value="">กรุณาเลือก</option>
                                <?php foreach ($ArrGroup as $item):?>
                                <option value="<?php echo $item['id'];?>" <?php echo $item['id']==$thisProductGroupId?'selected':'';?>><?php echo $item['group_name'];?></option>
                                <?php endforeach;?>
                            </select>
                            <button class="btn btn-outline-warning" type="button" onclick="modalGroupShow()">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                    </div>
                    <div class="row g-3">
                        <div class="col-md-8">
                            <label for="inputProductName">สินค้า</label>
                            <input type="text" class="form-control" id="inputProductName" name="product_name" value="<?php echo $thisProductName;?>" required>
                        </div>
                        <div class="col-md-4">
                            <label for="inputProductPrice">ราคา</label>
                            <input type="number" class="form-control" id="inputProductPrice" name="price" value="<?php echo $thisProductPrice;?>" required>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="inputProductDetail">รายละเอียด</label>
                        <textarea class="form-control" id="inputProductDetail" name="detail"><?php echo $thisProductDetail;?></textarea>
                    </div>
                    <div class="text-center">
                        <input id="inputProductImg" class="d-none" type="text" name="img" value="<?php echo $thisProductImg;?>" placeholder="">
                        <input class="d-none" type="text" name="action" value="saveProduct" placeholder="">

                        <?php if($thisProductId!='-'):?>
                            <input class="d-none" type="text" name="action" value="editProduct" placeholder="">
                            <button class="btn btn-warning" type="submit"><i class="fas fa-edit"></i> แก้ไข</button>
                        <?php else:?>
                            <input class="d-none" type="text" name="action" value="saveProduct" placeholder="">
                            <button class="btn btn-success" type="submit"><i class="fas fa-save"></i> บันทึก</button>
                        <?php endif;?>
                    </div>

                </form>

            </div>

        </div>
    </div>

</div>
<!-- ____________ End: Body ____________ -->


<!-- =========== Start: Modal =========== -->
<!-- modal group -->
<div class="modal fade" id="modalGroup" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalGroupLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalGroupLabel">จัดการกลุ่ม/ประเภท สินค้า </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form class="row g-3" method="post">
                    <div class="col">
                        <label for="mGroupNameInsert" class="visually-hidden">Group name</label>
                        <input type="text" class="form-control" id="mGroupNameInsert" placeholder="กลุ่ม/ประเภท" name="group_name" required>
                    </div>
                    <div class="col-auto">
                        <input class="d-none" type="text" name="action" value="saveGroup" placeholder="">
                        <button type="submit" class="btn btn-outline-success mb-3"><i class="fas fa-save"></i> บันทึก</button>
                    </div>
                </form>

                <div class="table-responsive">
                    <table class="table table-borderless table-sm text-nowrap w-100">
                        <tbody>
                        <?php foreach ($ArrGroup as $key=>$item):?>
                        <tr>
                            <td>
                                <form class="row g-3" method="post">
                                    <div class="col">
                                        <label for="mGroupNameUpdate-<?php echo $item['id'];?>" class="visually-hidden">Group name</label>
                                        <input type="text" class="form-control" id="mGroupNameUpdate-<?php echo $item['id'];?>" name="group_name" value="<?php echo $item['group_name'];?>" placeholder="กลุ่ม/ประเภท" required>
                                    </div>
                                    <div class="col-auto">
                                        <input class="d-none" type="text" name="id" value="<?php echo $item['id'];?>" placeholder="">
                                        <input class="d-none" type="text" name="action" value="editGroup" placeholder="">
                                        <button type="submit" class="btn btn-outline-warning mb-3"><i class="fas fa-edit"></i> แก้ไข</button>
                                    </div>
                                </form>
                            </td>
                            <td>
                                <button class="btn btn-outline-danger" type="button" onclick="modalDeleteGroupShow('<?php echo $item['id'];?>','<?php echo $item['group_name'];?>')">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal delete -->
<div class="modal fade" id="modalDelete" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content" method="post">
            <div class="modal-header">
                <h5 class="modal-title text-danger" id="modalDeleteLabel">ลบข้อมูล</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="text-center mb-3">
                    <i class="fas fa-ban fa-4x text-danger"></i>
                </div>
                <div class="text-center text-danger">
                    <strong>Delete</strong>
                    <p id="mDelText">ลบข้อมูล [สินค้า]</p>
                </div>
            </div>
            <div class="modal-footer">
                <input id="mDelId" class="d-none" type="text" name="id" value="" placeholder="">
                <input class="d-none" type="text" name="action" value="delGroup" placeholder="">

                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>
    </div>
</div>
<!-- ____________ End: Modal ____________ -->


<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_adminScript.php';?>
<!-- ____________ End: Script ____________ -->


<!-- =========== Start: Custom Script =========== -->
<script>

    //modal show group
    function modalGroupShow() {
        $("#modalGroup").modal("show");
    }
    function modalDeleteGroupShow(id,text) {
        $("#modalGroup").modal("hide");
        $("#modalDelete").modal("show");

        $("#mDelText").html("ลบข้อมูล "+text);
        $("#mDelId").val(id);
    }


    //upload image
    let ajax_img;
    function imageUpload(input) {
        if (input.files && input.files[0]) {
            ajax_img = new XMLHttpRequest();

            $('#imageUploadProgressbarId').removeClass('d-none');

            let form_data = new FormData();
            form_data.append("fileToUpload", input.files[0]);
            ajax_img.upload.addEventListener("progress", progressHandler, false);
            ajax_img.addEventListener("load", completeHandler, false);
            ajax_img.addEventListener("error", errorHandler, false);
            ajax_img.addEventListener("abort", abortHandler, false);
            ajax_img.open("POST","/upload/upload_file.php");
            ajax_img.send(form_data);

            function progressHandler(event) {
                let percent = (event.loaded / event.total) * 100;
                $("#progressbarId").css('width', Math.round(percent) + "%");
            }

            function completeHandler(event) {
                let data_return = JSON.parse(event.target.responseText);
                if (data_return['status'] === 'ok') {
                    let src = '/upload/uploads/'+data_return['new_name'];

                    $("#progressbarId").css('width', "0%");
                    $("#imageUploadProgressbarId").addClass("d-none");

                    $("#pathImageUploadId").attr("src",src);

                    $("#inputProductImg").val(src);

                } else {
                    ajax_img.abort();
                    $("#progressbarId").css('width', "0%");
                    $("#imageUploadProgressbarId").addClass("d-none");
                    alert("Error:" + data_return['message']);
                }
            }

            function errorHandler(event) {
                ajax_img.abort();
                $("#progressbarId").css('width', "0%");
                $("#imageUploadProgressbarId").addClass("d-none");
                alert("Upload Failed");
            }

            function abortHandler(event) {
                ajax_img.abort();
                $("#progressbarId").css('width', "0%");
                $("#imageUploadProgressbarId").addClass("d-none");
                alert("Upload Aborted");
            }
        }
        else {
            alert("Not found file input!!!");
        }
    }
    function cancelUpload() {
        if(ajax_img!=null){
            ajax_img.abort();
        }
        $("#progressbarId").css('width', "0%");
        $("#imageUploadProgressbarId").addClass("d-none");
        alert("Cancel Upload Image");
    }

</script>
<!-- ____________ End: Custom Script ____________ -->


</body>

</html>

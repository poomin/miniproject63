<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/18/2021
 * Time: 9:56 AM
 */
require_once __DIR__.'/_session.php';
require_once __DIR__.'/controller/pageIndexController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_header.php';?>

</head>
<body>


<!-- =========== Start: Top Menu =========== -->
<div class="container fixed-top">
    <?php require_once __DIR__.'/_menu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="container p-body">

    <!-- slide image -->
    <div id="carouselImg" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">

            <?php foreach ($PRODUCTS as $key=>$item):?>
            <div class="carousel-item <?php echo $key==0?'active':'';?>">
                <img src="<?php echo $item['img'];?>" class="d-block w-100">
                <div class="carousel-caption bg-8 rounded">
                    <span class="d-block"><?php echo $item['product_name'];?></span>
                    <span class="d-block">
                        <i class="fa fa-tag t-d"></i>
                        <strong class="px-2"><?php echo $item['price'];?></strong> บาท
                    </span>
                </div>
            </div>
            <?php endforeach;?>

        </div>
        <button class="carouse-pre" type="button" data-bs-target="#carouselImg"  data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        </button>
        <button class="carouse-next" type="button" data-bs-target="#carouselImg"  data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
        </button>
    </div>

    <!-- list order payment -->
    <div class="shadow p-2 mt-5 bg-white bg-doc">
        <div class="p-2">
            <h5 class="text-secondary pb-2 border-bottom"><i class="fas fa-cookie-bite mx-2"></i> เปี๊ยะ ฟิน ฟิน จร้า ^^</h5>
        </div>
        <div class="p-2">
            <div class="container-fluid">
                <div class="row row-cols-2 row-cols-md-4 g-3">

                    <?php foreach ($PRODUCTS as $key=>$item):?>
                    <div class="col card-box-news p-1" style="cursor: pointer;" onclick="window.location.href='page-view.php?id=<?php echo $item['id'];?>';">
                        <div class="card shadow bg-cake">
                            <img src="<?php echo $item['img'];?>" class="card-img-top card-gallery-img">
                            <div class="card-body p-1">
                                <small class="card-title text-center"><?php echo $item['product_name'];?></small>
                                <div class="clearfix">
                                    <div class="float-end">
                                        <span class="small badge bg-danger"><?php echo $item['price'];?> -</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>

                </div>
            </div>
        </div>
    </div>

    <!-- connection -->
    <div class="row mt-5">
        <div class="col-md-4">
            <div class="container shadow bg-white bg-comment p-2">
                <div class="row align-items-center">
                    <div class="col-12 pb-2 mb-1 text-muted border-bottom">
                        <i class="fas fa-users"></i> ติดต่อแม่ค้า <?php echo isset($SHOP['shop_owner'])?$SHOP['shop_owner']:'';?>
                    </div>
                    <div class="col-auto text-center">
                        <img class="img-fluid" src="img/icon-female.png" style="max-height: 120px;" >
                    </div>
                    <div class="col">
                        <div><i class="fa fa-phone text-info"></i>
                            <?php echo isset($SHOP['tel'])?$SHOP['tel']:'';?>
                        </div>
                        <div> <i class="fab fa-line text-success"></i>
                            <a href="<?php echo isset($SHOP['line_link'])?$SHOP['line_link']:'#';?>"><?php echo isset($SHOP['line_name'])?$SHOP['line_name']:'';?> </a>
                        </div>
                        <div>
                            <i class="fab fa-facebook-f text-primary"></i>
                            <a href="<?php echo isset($SHOP['fb_link'])?$SHOP['fb_link']:'#';?>" target="_blank"><?php echo isset($SHOP['fb_name'])?$SHOP['fb_name']:'';?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="shadow bg-white bg-comment p-2">
                <div class="pb-2 mb-1 text-muted border-bottom">
                    <i class="fa fa-qrcode"></i> Line QrCode
                </div>
                <div class="text-center">
                    <img class="img-fluid" src="<?php echo isset($SHOP['line_qrcode'])?$SHOP['line_qrcode']:'img/df-upload.png';?>" style="max-height: 120px;">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="container shadow bg-white bg-comment p-2">
                <div class="row align-items-center">
                    <div class="col-12 pb-2 mb-1 text-muted border-bottom">
                        <i class="fas fa-map-marker"></i> ที่อยู่ติดต่อ
                    </div>
                    <div class="col-auto text-center">
                        <img class="img-fluid" src="img/df-map.png" style="max-height: 120px;" >
                    </div>
                    <div class="col">
                        <?php echo isset($SHOP['address'])?$SHOP['address']:'';?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- map -->
    <div class="shadow p-2 mt-5 mb-5 bg-white">
        <div class="p-2">
            <h5 class="text-secondary pb-2 border-bottom"><i class="fas fa-map-marker mx-2"></i> ที่อยู่ร้าน </h5>
        </div>
        <!--Google map-->
        <iframe src="<?php echo isset($SHOP['google_map'])?$SHOP['google_map']:'';?>" frameborder="0" style="border:0;height:300px;width:100%;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        <!--Google Maps-->
    </div>

</div>
<!-- ____________ End: Body ____________ -->


<!-- =========== Start: Footer =========== -->
<div class="container-fluid footer pt-4 pb-4 bg-8">
    <?php require_once __DIR__.'/_footer.php';?>
</div>
<!-- ____________ End: Footer ____________ -->


<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_script.php';?>
<!-- ____________ End: Script ____________ -->

<!-- modal product shop -->
<?php require_once __DIR__.'/_modalProduct.php';?>


<!-- =========== Start: Custom Script =========== -->
<script>


</script>
<!-- ____________ End: Custom Script ____________ -->


</body>
</html>

<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:38 AM
 */
require_once __DIR__.'/_DB.php';
class BillModel extends _DB
{
    private $TB = 'bill';


    function insertThis($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(member_id,member_name,member_address,member_tel,bill_type,bill_price,bill_date,bill_note,shipping_name,shipping_price,bank_code,bank_number,slip_img)";
        $sqlValue .= " VALUES (:member_id,:member_name,:member_address,:member_tel,:bill_type,:bill_price,:bill_date,:bill_note,:shipping_name,:shipping_price,:bank_code,:bank_number,:slip_img)";
        $sqlParams = [
            ':member_id'=>$attr['member_id'],
            ':member_name'=>$attr['member_name'],
            ':member_address'=>$attr['member_address'],
            ':member_tel'=>$attr['member_tel'],
            ':bill_type'=>$attr['bill_type'],
            ':bill_price'=>$attr['bill_price'],
            ':bill_date'=>$attr['bill_date'],
            ':bill_note'=>$attr['bill_note'],
            ':shipping_name'=>$attr['shipping_name'],
            ':shipping_price'=>$attr['shipping_price'],
            ':bank_code'=>$attr['bank_code'],
            ':bank_number'=>$attr['bank_number'],
            ':slip_img'=>$attr['slip_img']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function updateStatus($attr,$id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " SET bill_type=:bill_type , bill_note=:bill_note";
        $sqlValue .= " WHERE id=:id";
        $sqlParams = [
            ':bill_type'=> $attr['bill_type'],
            ':bill_note'=> $attr['bill_note'],
            ':id'=> $id
        ];
        $sql = "UPDATE $thisTable $sqlValue";
        $returnData = $this->update($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }
    function updateCode($attr,$id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " SET shipping_code=:shipping_code , bill_type='S' ";
        $sqlValue .= " WHERE id=:id";
        $sqlParams = [
            ':shipping_code'=> $attr['shipping_code'],
            ':id'=> $id
        ];
        $sql = "UPDATE $thisTable $sqlValue";
        $returnData = $this->update($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function selectThis(){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "";
        $sqlParams = [];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;
    }
    function selectThisId($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE id=:id";
        $sqlParams = [
            ':id'=> $id
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->query($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;
    }

    function selectThisAllMemberId($memberId){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE member_id=:member_id";
        $sqlParams = [
            ':member_id'=> $memberId
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;
    }



}
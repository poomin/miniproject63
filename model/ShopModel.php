<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:38 AM
 */
require_once __DIR__.'/_DB.php';
class ShopModel extends _DB
{
    private $TB = 'shop';


    function insertUpdateShop($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(id,shop_name,shop_owner,tel)";
        $sqlValue .= " VALUES (:id,:shop_name,:shop_owner,:tel)";
        $sqlValue .= " ON DUPLICATE KEY UPDATE shop_name=:c_shop_name, shop_owner=:c_shop_owner, tel=:c_tel";
        $sqlParams = [
            ':id'=>$attr['id'],
            ':shop_name'=>$attr['shop_name'],
            ':shop_owner'=>$attr['shop_owner'],
            ':tel'=>$attr['tel'],
            ':c_shop_name'=>$attr['shop_name'],
            ':c_shop_owner'=>$attr['shop_owner'],
            ':c_tel'=>$attr['tel']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }
    function insertUpdateFb($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(id,fb_name,fb_link)";
        $sqlValue .= " VALUES (:id,:fb_name,:fb_link)";
        $sqlValue .= " ON DUPLICATE KEY UPDATE fb_name=:c_fb_name, fb_link=:c_fb_link";
        $sqlParams = [
            ':id'=>$attr['id'],
            ':fb_name'=>$attr['fb_name'],
            ':fb_link'=>$attr['fb_link'],
            ':c_fb_name'=>$attr['fb_name'],
            ':c_fb_link'=>$attr['fb_link']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }
    function insertUpdateLine($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(id,line_name,line_link)";
        $sqlValue .= " VALUES (:id,:line_name,:line_link)";
        $sqlValue .= " ON DUPLICATE KEY UPDATE line_name=:c_line_name, line_link=:c_line_link";
        $sqlParams = [
            ':id'=>$attr['id'],
            ':line_name'=>$attr['line_name'],
            ':line_link'=>$attr['line_link'],
            ':c_line_name'=>$attr['line_name'],
            ':c_line_link'=>$attr['line_link']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }
    function insertUpdateAddress($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(id,address)";
        $sqlValue .= " VALUES (:id,:address)";
        $sqlValue .= " ON DUPLICATE KEY UPDATE address=:c_address";
        $sqlParams = [
            ':id'=>$attr['id'],
            ':address'=>$attr['address'],
            ':c_address'=>$attr['address']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }
    function insertUpdateMap($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(id,google_map)";
        $sqlValue .= " VALUES (:id,:google_map)";
        $sqlValue .= " ON DUPLICATE KEY UPDATE google_map=:c_google_map";
        $sqlParams = [
            ':id'=>$attr['id'],
            ':google_map'=>$attr['google_map'],
            ':c_google_map'=>$attr['google_map']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }
    function insertUpdateQrCode($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(id,line_qrcode)";
        $sqlValue .= " VALUES (:id,:line_qrcode)";
        $sqlValue .= " ON DUPLICATE KEY UPDATE line_qrcode=:c_line_qrcode";
        $sqlParams = [
            ':id'=>$attr['id'],
            ':line_qrcode'=>$attr['line_qrcode'],
            ':c_line_qrcode'=>$attr['line_qrcode']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function selectThisId($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE id=:id";
        $sqlParams = [
            ':id'=>$id,
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->query($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;

    }





}
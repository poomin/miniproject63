<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:38 AM
 */
require_once __DIR__.'/_DB.php';
class ProductCommentModel extends _DB
{
    private $TB = 'product_comment';

    function insertThis($attr){
        date_default_timezone_set("Asia/Bangkok");
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(product_id,comment_name,comment_post,create_at)";
        $sqlValue .= " VALUES (:product_id,:comment_name,:comment_post,:create_at)";
        $sqlParams = [
            ':product_id'=>$attr['product_id'],
            ':comment_name'=>$attr['comment_name'],
            ':comment_post'=>$attr['comment_post'],
            ':create_at'=> date("Y-m-d H:i:s")
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function selectThisId($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE id=:id";
        $sqlParams = [
            ':id'=>$id,
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->query($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;

    }

    function selectThisAllProductId($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE product_id=:product_id";
        $sqlParams = [
            ':product_id'=>$id,
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;

    }





}
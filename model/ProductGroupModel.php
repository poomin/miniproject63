<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:38 AM
 */
require_once __DIR__.'/_DB.php';
class ProductGroupModel extends _DB
{
    private $TB = 'product_group';


    function insertThis($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(group_name,this_remove)";
        $sqlValue .= " VALUES (:group_name,:this_remove)";
        $sqlParams = [
            ':group_name'=>$attr['group_name'],
            ':this_remove'=>$attr['this_remove']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function updateThis($attr,$id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " SET group_name=:group_name";
        $sqlValue .= " WHERE id=:id";
        $sqlParams = [
            ':group_name'=>$attr['group_name'],
            ':id'=> $id
        ];
        $sql = "UPDATE $thisTable $sqlValue";
        $returnData = $this->update($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function updateDeleteThis($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " SET this_remove=:this_remove";
        $sqlValue .= " WHERE id=:id";
        $sqlParams = [
            ':this_remove'=> 'Y',
            ':id'=> $id
        ];
        $sql = "UPDATE $thisTable $sqlValue";
        $returnData = $this->update($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }


    function selectThisAll(){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE this_remove=:this_remove ";
        $sqlParams = [
            ':this_remove'=> 'N'
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;

    }





}
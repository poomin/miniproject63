<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:38 AM
 */
require_once __DIR__.'/_DB.php';
class BillListModel extends _DB
{
    private $TB = 'bill_list';


    function insertThis($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(product_id,member_id,product_name,product_price,order_number,order_note)";
        $sqlValue .= " VALUES (:product_id,:member_id,:product_name,:product_price,:order_number,:order_note)";
        $sqlParams = [
            ':product_id'=>$attr['product_id'],
            ':member_id'=>$attr['member_id'],
            ':product_name'=>$attr['product_name'],
            ':product_price'=>$attr['product_price'],
            ':order_number'=>$attr['order_number'],
            ':order_note'=>$attr['order_note']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function updateThisCartByMemberId($memberId,$billId){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " SET bill_id=:bill_id ";
        $sqlValue .= " WHERE member_id=:member_id AND bill_id=0";
        $sqlParams = [
            ':bill_id'=> $billId,
            ':member_id'=> $memberId
        ];
        $sql = "UPDATE $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function deleteThis($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE id=:id";
        $sqlParams = [
            ':id'=>$id
        ];
        $sql = "DELETE FROM $thisTable $sqlValue";
        $returnData = $this->update($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }


    function selectByBillId($BillId){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE bill_id=:bill_id";
        $sqlParams = [
            ':bill_id'=> $BillId
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();

        $pKey = [];
        if($returnData['status']){
            require_once __DIR__.'/ProductModel.php';
            $MP = new ProductModel();
            $r = $MP->selectThisAll();
            if($r['status']){
                foreach ($r['result'] as $i){
                    $pKey[$i['id']] = $i['img'];
                }
            }
        }
        if($returnData['status']){
            foreach ($returnData['result'] as $key=>$item){
                $iId = $item['product_id'];
                $returnData['result'][$key]['img'] = '/img/df-upload.png';
                if(isset($pKey[$iId])){
                    $returnData['result'][$key]['img'] = $pKey[$iId];
                }
            }
        }


        return $returnData;
    }
    function selectCartByMemberId($memberId){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE member_id=:member_id AND bill_id=0";
        $sqlParams = [
            ':member_id'=> $memberId
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();

        $pKey = [];
        if($returnData['status']){
            require_once __DIR__.'/ProductModel.php';
            $MP = new ProductModel();
            $r = $MP->selectThisAll();
            if($r['status']){
                foreach ($r['result'] as $i){
                    $pKey[$i['id']] = $i['img'];
                }
            }
        }
        if($returnData['status']){
            foreach ($returnData['result'] as $key=>$item){
                $iId = $item['product_id'];
                $returnData['result'][$key]['img'] = '/img/df-upload.png';
                if(isset($pKey[$iId])){
                    $returnData['result'][$key]['img'] = $pKey[$iId];
                }
            }
        }


        return $returnData;
    }
    function countByMemberId($memberId){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE member_id=:member_id AND bill_id=0";
        $sqlParams = [
            ':member_id'=> $memberId
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();

        $returnCount = 0;
        if($returnData['status']){
            $returnCount = count($returnData['result']);
        }

        return $returnCount;
    }




}
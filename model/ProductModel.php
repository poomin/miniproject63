<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:38 AM
 */
require_once __DIR__.'/_DB.php';
class ProductModel extends _DB
{
    private $TB = 'product';


    function insertThis($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(group_id,product_name,price,img,detail,this_remove)";
        $sqlValue .= " VALUES (:group_id, :product_name, :price, :img, :detail, :this_remove)";
        $sqlParams = [
            ':group_id'=>$attr['group_id'],
            ':product_name'=>$attr['product_name'],
            ':price'=>$attr['price'],
            ':img'=>$attr['img'],
            ':detail'=>$attr['detail'],
            ':this_remove'=>$attr['this_remove']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function updateThis($attr,$id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " SET group_id=:group_id, product_name=:product_name, price=:price, img=:img, detail=:detail, this_remove=:this_remove";
        $sqlValue .= " WHERE id=:id";
        $sqlParams = [
            ':group_id'=>$attr['group_id'],
            ':product_name'=>$attr['product_name'],
            ':price'=>$attr['price'],
            ':img'=>$attr['img'],
            ':detail'=>$attr['detail'],
            ':this_remove'=>$attr['this_remove'],
            ':id'=> $id
        ];
        $sql = "UPDATE $thisTable $sqlValue";
        $returnData = $this->update($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function updateDeleteThis($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " SET this_remove=:this_remove";
        $sqlValue .= " WHERE id=:id";
        $sqlParams = [
            ':this_remove'=> 'Y',
            ':id'=> $id
        ];
        $sql = "UPDATE $thisTable $sqlValue";
        $returnData = $this->update($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }



    function selectThisId($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE id=:id AND this_remove=:this_remove ";
        $sqlParams = [
            ':id'=> $id,
            ':this_remove'=> 'N'
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->query($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;
    }

    function selectThisAll(){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE this_remove=:this_remove ";
        $sqlParams = [
            ':this_remove'=> 'N'
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;
    }
    function selectThisSearch($search){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE product_name LIKE :product_name ";
        $sqlParams = [
            ':product_name'=> "%$search%"
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;
    }


}
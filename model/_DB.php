<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:26 AM
 */

class _DB
{
    private $servername = "localhost";
    private $username = "root";
    private $password = "1234";
    private $dbname = "db_mini_project_shop";
    private $conn = null;
    private $stmt = null;

    function connect(){
        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->conn->exec("SET NAMES utf8");
            $this->conn->exec("SET time_zone ='Asia/Bangkok'");
            $this->conn->exec("SET profiling = 1;");
        }
        catch(PDOException $e)
        {
            header( "location: /model/_sql-error.php?error=".$e->getMessage());
            exit(0);
        }
        return $this->conn;

    }

    function close()
    {
        try {
            $this->stmt = null;
            $this->conn = null;
        } catch (Exception $e) {
            header( "location: /model/_sql-error.php?error=".$e->getMessage());
            exit(0);
        } finally {
            //$this->conn = null;
        }
    }

    function insert($sql, $arr)
    {
        //sql pattern is INSERT INTO Table(name, colour) VALUES (?, ?)
        $checkProcess = true;
        $returnData = [
            'status'=> true,
            'message'=> 'success',
            'result'=> []
        ];
        try {

            $this->stmt = $this->conn->prepare($sql);
            //check prepare
            if(! $this->stmt ){
                $error = $this->conn->errorInfo();
                $returnData['status'] = false;
                $returnData['message'] = (count($error)>=3)? $error[2] : implode(",",$error);
                $checkProcess = false;
            }

            //check execute
            if($checkProcess){
                $success = $this->stmt->execute($arr);
                if(! $success ){
                    $error = $this->stmt->errorInfo();
                    $returnData['status'] = false;
                    $returnData['message'] = (count($error)>=3)? $error[2] : implode(",",$error );
                    $checkProcess = false;
                }
            }

            //success
            if($checkProcess){
                $last_id = $this->conn->lastInsertId();
                $returnData['status'] = true;
                $returnData['result'] = $last_id;
            }

        }
        catch (Exception $e) {
            $returnData['status'] = false;
            $returnData['message'] = $e->getMessage();
        }
        return $returnData;
    }

    function update($sql, $arr)
    {
        //sql pattern is UPDATE Table SET name = ? , colour = ?
        $checkProcess = true;
        $returnData = [
            'status'=> true,
            'message'=> 'success',
            'result'=> []
        ];

        try {

            $this->stmt = $this->conn->prepare($sql);
            //check prepare
            if(! $this->stmt ){
                $error = $this->conn->errorInfo();
                $returnData['status'] = false;
                $returnData['message'] = count($error)>=3?$error[2]: implode(",",$error);
                $checkProcess = false;
            }

            //check execute
            if($checkProcess){
                $success = $this->stmt->execute($arr);
                if(! $success ){
                    $error = $this->stmt->errorInfo();
                    $returnData['status'] = false;
                    $returnData['message'] = (count($error)>=3)? $error[2] : implode(",",$error);
                    $checkProcess = false;
                }
            }

            //success
            if($checkProcess){
                $affectedRows = $this->stmt->rowCount();
                $returnData['status'] = true;
                $returnData['result'] = $affectedRows;
            }

        }
        catch (Exception $e) {
            $returnData['status'] = false;
            $returnData['result'] = $e->getMessage();
        }
        return $returnData;
    }

    function query($sql, $params)
    {
        // PDO, prepared statement
        $checkProcess = true;
        $returnData = [
            'status'=> true,
            'message'=> 'success',
            'result'=> []
        ];
        try {
            $this->stmt = $this->conn->prepare($sql);
            //check prepare
            if(! $this->stmt ){
                $error = $this->conn->errorInfo();
                $returnData['status'] = false;
                $returnData['message'] = count($error)>=3?$error[2]: implode(",",$error);
                $checkProcess = false;
            }

            //check execute
            if($checkProcess){
                $success = $this->stmt->execute($params);
                if(! $success ){
                    $error = $this->stmt->errorInfo();
                    $returnData['status'] = false;
                    $returnData['message'] = (count($error)>=3)? $error[2] : implode(",",$error);
                    $checkProcess = false;
                }
            }

            //success
            if($checkProcess){
                $res = $this->stmt->fetch(PDO::FETCH_ASSOC);
                $returnData['status'] = true;
                $returnData['result'] = ($res)?$res : [];
            }

        }
        catch (Exception $e) {
            $returnData['status'] = false;
            $returnData['result'] = $e->getMessage();
        }
        return $returnData;
    }

    function queryAll($sql, $params)
    {

        // PDO, prepared statement
        $checkProcess = true;
        $returnData = [
            'status'=> true,
            'message'=> 'success',
            'result'=> []
        ];
        try {
            $this->stmt = $this->conn->prepare($sql);
            //check prepare
            if(! $this->stmt ){
                $error = $this->conn->errorInfo();
                $returnData['status'] = false;
                $returnData['message'] = count($error)>=3?$error[2]: implode(",",$error);
                $checkProcess = false;
            }

            //check execute
            if($checkProcess){
                $success = $this->stmt->execute($params);
                if(! $success ){
                    $error = $this->stmt->errorInfo();
                    $returnData['status'] = false;
                    $returnData['message'] = (count($error)>=3)? $error[2] : implode(",",$error);
                    $checkProcess = false;
                }
            }

            //success
            if($checkProcess){
                $res = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
                $returnData['status'] = true;
                $returnData['result'] = ($res)?$res : [];
            }

        }
        catch (Exception $e) {
            $returnData['status'] = false;
            $returnData['result'] = $e->getMessage();
        }
        return $returnData;

    }

    function queryNoParams($sql)
    {

        // PDO, prepared statement
        $checkProcess = true;
        $returnData = [
            'status'=> true,
            'message'=> 'success',
            'result'=> []
        ];
        try {
            $this->stmt = $this->conn->prepare($sql);
            //check prepare
            if(! $this->stmt ){
                $error = $this->conn->errorInfo();
                $returnData['status'] = false;
                $returnData['message'] = count($error)>=3?$error[2]: implode(",",$error);
                $checkProcess = false;
            }

            //check execute
            if($checkProcess){
                $success = $this->stmt->execute();
                if(! $success ){
                    $error = $this->stmt->errorInfo();
                    $returnData['status'] = false;
                    $returnData['message'] = (count($error)>=3)? $error[2] : implode(",",$error);
                    $checkProcess = false;
                }
            }

            //success
            if($checkProcess){
                $res = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
                $returnData['status'] = true;
                $returnData['result'] = ($res)?$res : [];
            }

        }
        catch (Exception $e) {
            $returnData['status'] = false;
            $returnData['result'] = $e->getMessage();
        }
        return $returnData;

    }
}
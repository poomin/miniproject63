<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:38 AM
 */
require_once __DIR__.'/_DB.php';
class ProductUploadModel extends _DB
{
    private $TB = 'product_upload';


    function insertThis($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(product_id,img)";
        $sqlValue .= " VALUES (:product_id,:img)";
        $sqlParams = [
            ':product_id'=>$attr['product_id'],
            ':img'=>$attr['img']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }


    function selectThisId($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE id=:id";
        $sqlParams = [
            ':id'=>$id,
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->query($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;

    }

    function selectThisAllProductId($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE product_id=:product_id";
        $sqlParams = [
            ':product_id'=>$id,
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;

    }





}
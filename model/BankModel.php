<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:38 AM
 */
require_once __DIR__.'/_DB.php';
class BankModel extends _DB
{
    private $TB = 'bank';

    function insertThis($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(bank_code,bank_number,bank_account)";
        $sqlValue .= " VALUES (:bank_code,:bank_number,:bank_account)";
        $sqlParams = [
            ':bank_code'=>$attr['bank_code'],
            ':bank_number'=>$attr['bank_number'],
            ':bank_account'=>$attr['bank_account']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function updateThis($attr,$id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " SET bank_code=:bank_code, bank_number=:bank_number, bank_account=:bank_account";
        $sqlValue .= " WHERE id=:id";
        $sqlParams = [
            ':bank_code'=>$attr['bank_code'],
            ':bank_number'=>$attr['bank_number'],
            ':bank_account'=>$attr['bank_account'],
            ':id'=> $id
        ];
        $sql = "UPDATE $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function deleteThis($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE id=:id";
        $sqlParams = [
            ':id'=>$id
        ];
        $sql = "DELETE FROM $thisTable $sqlValue";
        $returnData = $this->update($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }

    function selectThis(){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE this_remove=:this_remove";
        $sqlParams = [
            ':this_remove'=> 'N',
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;

    }

    function selectThisId($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE id=:id";
        $sqlParams = [
            ':id'=> $id,
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->query($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;

    }

}
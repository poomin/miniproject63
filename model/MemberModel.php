<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:38 AM
 */
require_once __DIR__.'/_DB.php';
class MemberModel extends _DB
{
    private $TB = 'member';


    function insertThis($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = "(username,password,member_name,member_type,address,tel,this_remove)";
        $sqlValue .= " VALUES (:username,:password,:member_name,:member_type,:address,:tel,:this_remove)";
        $sqlParams = [
            ':username'=>$attr['username'],
            ':password'=>$attr['password'],
            ':member_name'=>$attr['member_name'],
            ':member_type'=>$attr['member_type'],
            ':address'=>$attr['address'],
            ':tel'=>$attr['tel'],
            ':this_remove'=>$attr['this_remove']
        ];
        $sql = "INSERT INTO $thisTable $sqlValue";
        $returnData = $this->insert($sql,$sqlParams);

        //close DB
        $this->close();
        return $returnData;
    }


    function loginThis($attr){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE username=:username AND password=:password AND this_remove=:this_remove ";
        $sqlParams = [
            ':username'=>$attr['username'],
            ':password'=>$attr['password'],
            ':this_remove'=>$attr['this_remove']
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->query($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;

    }
    function selectThisId($id){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE id=:id AND this_remove=:this_remove ";
        $sqlParams = [
            ':id'=>$id,
            ':this_remove'=> 'N'
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->query($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;

    }

    function selectMember(){
        $thisTable = $this->TB;

        //connect DB
        $this->connect();
        $sqlValue = " WHERE member_type=:member_type AND this_remove=:this_remove ";
        $sqlParams = [
            ':member_type'=> 'A',
            ':this_remove'=> 'N'
        ];
        $sql = "SELECT * FROM $thisTable ".$sqlValue;
        $returnData = $this->queryAll($sql,$sqlParams);
        //close DB
        $this->close();


        return $returnData;

    }



}
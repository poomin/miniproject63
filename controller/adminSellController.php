<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 11:28 AM
 */


/* ========= require model ========= */
require_once __DIR__.'/../model/BillModel.php';
$MBill = new BillModel();



/* ========= parameter set ========= */
$thisAlertStatus = '';  //E:error,S:success
$thisAlertText = '';

$billWait = [];
$billSend = [];
$billAll = [];

$countWait = 0;
$countSend = 0;


/* ========= action method ========= */
$action = isset($_POST['action'])?$_POST['action']:'';
if($action == 'confirm'){

    $reqBillType = isset($_POST['bill_type'])?$_POST['bill_type']:'';
    $reqBillNote = isset($_POST['bill_note'])?$_POST['bill_note']:'';
    $reqBillId = isset($_POST['bill_id'])?$_POST['bill_id']:'';

    $attr=[
        'bill_type'=> $reqBillType,
        'bill_note'=> $reqBillNote,
    ];
    $res = $MBill->updateStatus($attr,$reqBillId);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='ยืนยันรายการสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}
elseif($action == 'ship'){

    $reqBillCode = isset($_POST['shipping_code'])?$_POST['shipping_code']:'';
    $reqBillId = isset($_POST['bill_id'])?$_POST['bill_id']:'';

    $attr=[
        'shipping_code'=> $reqBillCode
    ];
    $res = $MBill->updateCode($attr,$reqBillId);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='ยืนยันรายการสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}




/* ========= page view ========= */
$res = $MBill->selectThis();
if($res['status']){
    $result = $res['result'];

    foreach ($result as $key=>$item){
        if($item['bill_type']=='B'){
            $billWait[] = $item;
        }
        elseif($item['bill_type']=='O' || $item['bill_type']=='P' ){
            $billSend[] = $item;
        }
        else{
            $billAll[] = $item;
        }
    }
}

$countWait = count($billWait);
$countSend = count($billSend);


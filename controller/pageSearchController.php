<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/18/2021
 * Time: 10:00 AM
 */

/* ========= require model ========= */
require_once __DIR__.'/../model/ProductModel.php';
require_once __DIR__.'/../model/ProductGroupModel.php';
require_once __DIR__.'/../model/ShopModel.php';
$MProduct = new ProductModel();
$MPGroup = new ProductGroupModel();
$MShop = new ShopModel();

/* ========= parameter set ========= */
$PRODUCTS = [];
$SHOP = [];
$GROUPS = [];
$SEARCH = [];


/* ========= action method ========= */
$searchText = isset($_REQUEST['search'])?$_REQUEST['search']:'';


/* ========= page view ========= */
$SHOP = [];
$res = $MShop->selectThisId(1);
if($res['status']){
    $SHOP = $res['result'];
}
$GROUPS = [];
$res = $MPGroup->selectThisAll();
if($res['status']){
    $GROUPS = $res['result'];
}


$PRODUCTS = [];
$res = $MProduct->selectThisAll();
if($res['status']){
    $PRODUCTS = $res['result'];
}
$SEARCH = [];
$res = $MProduct->selectThisSearch($searchText);
if($res['status']){
    $SEARCH = $res['result'];
}

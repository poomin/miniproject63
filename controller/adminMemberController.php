<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 11:28 AM
 */


/* ========= require model ========= */
require_once __DIR__.'/../model/MemberModel.php';
$MMember = new MemberModel();



/* ========= parameter set ========= */
$thisAlertStatus = '';  //E:error,S:success
$thisAlertText = '';

$MEMBERS = [];


/* ========= action method ========= */

$action = isset($_POST['action'])?$_POST['action']:'';
if($action == 'action'){

    $reqX= isset($_POST['X'])?$_POST['X']:'';
    $res = [];
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='success';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];
    }

}



/* ========= page view ========= */
$MEMBERS = [];
$res = $MMember->selectMember();
if($res['status']){
    $MEMBERS = $res['result'];
}
<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 11:28 AM
 */

/* ========= require model ========= */
require_once __DIR__.'/../model/MemberModel.php';
require_once __DIR__.'/../model/ShopModel.php';
require_once __DIR__.'/../model/ProductGroupModel.php';
require_once __DIR__.'/../model/BillListModel.php';
$MMember = new MemberModel();
$MShop = new ShopModel();
$MPGroup = new ProductGroupModel();
$MBList = new BillListModel();


/* ========= parameter set ========= */
$alertStatus = false;
$alertMessage = '';

/* ========= action method ========= */
$action = isset($_POST['action'])?$_POST['action']:'';
if($action == 'addMember'){

    $reqUsername = isset($_POST['username'])?$_POST['username']:'';
    $reqPassword = isset($_POST['password'])?$_POST['password']:'';
    $reqMemberName = isset($_POST['member_name'])?$_POST['member_name']:'';
    $reqTel = isset($_POST['tel'])?$_POST['tel']:'';
    $reqAddress = isset($_POST['address'])?$_POST['address']:'';

    $attr=[
        'username'=> $reqUsername,
        'password'=> md5($reqPassword),
        'member_name'=> $reqMemberName,
        'address'=> $reqAddress,
        'tel'=> $reqTel,

        'member_type'=> 'U',
        'this_remove'=> 'N'
    ];
    $res = $MMember->insertThis($attr);
    if($res['status']){
        $_SESSION['SId'] = $res['result'];
        $_SESSION['SName'] = $reqUsername;
        $_SESSION['SType'] = 'U';
        header( "location: /page-user.php" );
        exit(0);
    }
    else{
        $alertStatus = true;
        $alertMessage = $res['message'];

    }

}
elseif($action == 'login'){
    $reqUsername = isset($_POST['username'])?$_POST['username']:'';
    $reqPassword = isset($_POST['password'])?$_POST['password']:'';

    $attr=[
        'username'=> $reqUsername,
        'password'=> md5($reqPassword),
        'this_remove'=> 'N'
    ];
    $res = $MMember->loginThis($attr);
    if($res['status']){
        $rObj = $res['result'];
        if(isset($rObj['id'])){
            if($rObj['member_type']=='A'){
                $_SESSION['SId'] = $rObj['id'];
                $_SESSION['SName'] = $rObj['username'];
                $_SESSION['SType'] = 'A';
                $_SESSION['SCount'] = 0;
                header( "location: /admin-home.php" );
                exit(0);
            }
            else{
                $_SESSION['SId'] = $rObj['id'];
                $_SESSION['SName'] = $rObj['username'];
                $_SESSION['SType'] = 'U';
                $_SESSION['SCount'] = $MBList->countByMemberId($rObj['id']);
                header( "location: /page-user.php" );
                exit(0);
            }
        }
        else{
            $alertStatus = true;
            $alertMessage = 'กรุณาตรวจสอบ Username & Password';
        }
    }
    else{
        $alertStatus = true;
        $alertMessage = $res['message'];

    }

}


/* ========= page view ========= */
$SHOP = [];
$res = $MShop->selectThisId(1);
if($res['status']){
    $SHOP = $res['result'];
}
$GROUPS = [];
$res = $MPGroup->selectThisAll();
if($res['status']){
    $GROUPS = $res['result'];
}

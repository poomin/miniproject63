<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 11:28 AM
 */


/* ========= require model ========= */
require_once __DIR__.'/../model/ProductGroupModel.php';
$MPGroup = new ProductGroupModel();
require_once __DIR__.'/../model/ProductModel.php';
$MProduct = new ProductModel();



/* ========= parameter set ========= */
$thisAlertStatus = '';  //E:error,S:success
$thisAlertText = '';

$ArrGroup = [];         // list product group
$ArrProduct = [];       // list product




/* ========= action method ========= */
$action = isset($_POST['action'])?$_POST['action']:'';
if($action == 'delProduct'){

    $reqId = isset($_POST['id'])?$_POST['id']:'';

    $res = $MProduct->updateDeleteThis($reqId);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}


/* ========= page view ========= */
$ArrGroup = [];
$res = $MPGroup->selectThisAll();
if($res['status']){
    $ArrGroup = $res['result'];
}



$ArrProduct = [];
$res = $MProduct->selectThisAll();
if($res['status']){
    $ArrProduct = $res['result'];

    $keyGroup = [];
    foreach ($ArrGroup as $item){
        $keyGroup[$item['id']] = $item['group_name'];
    }

    foreach ($ArrProduct as $key=>$item){
        $ArrProduct[$key]['group_name'] = '';
        if(isset($keyGroup[$item['group_id']])){
            $ArrProduct[$key]['group_name'] = $keyGroup[$item['group_id']];
        }
    }

}


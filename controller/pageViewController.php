<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/18/2021
 * Time: 10:00 AM
 */

/* ========= require model ========= */
require_once __DIR__.'/../model/ShopModel.php';
require_once __DIR__.'/../model/ProductModel.php';
require_once __DIR__.'/../model/ProductGroupModel.php';
require_once __DIR__.'/../model/ProductUploadModel.php';
require_once __DIR__.'/../model/ProductCommentModel.php';
require_once __DIR__.'/../model/BillListModel.php';
$MProduct = new ProductModel();
$MPGroup = new ProductGroupModel();
$MPUpload = new ProductUploadModel();
$MPComment = new ProductCommentModel();
$MBList = new BillListModel();
$MShop = new ShopModel();

/* ========= parameter set ========= */
//SESSION
$S_SID  = isset($S_SID)?$S_SID:'';
$S_SName  = isset($S_SName)?$S_SName:'';
$S_SType  = isset($S_SType)?$S_SType:'';
$S_SCount = isset($S_SCount)?$S_SCount:0;

//message warning
$alertStatus = false;
$alertType = '';// S:success,W:warn,D:danger
$alertText = '';

//page
$productId = '0';
$SHOP = [];
$GROUPS = [];
$PRODUCTS = [];
$PRODUCT =[];
$UPLOADS = [];
$COMMENTS = [];


/* ========= action method ========= */
$productId = isset($_REQUEST['id'])?$_REQUEST['id']:$productId;

$action = isset($_POST['action'])?$_POST['action']:'';
if($action=='comment'){
    $reqComment = isset($_POST['comment'])?$_POST['comment']:'';
    $reqName = isset($_POST['name'])?$_POST['name']:'';

    $attr=[
        'product_id'=> $productId,
        'comment_name'=> $reqName,
        'comment_post'=> $reqComment
    ];
    $res = $MPComment->insertThis($attr);
    if($res['status']){
        $alertStatus = true;
        $alertType='S';
        $alertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $alertStatus = true;
        $alertType='E';
        $alertText=$res['message'];
    }

}

elseif($action=='addProduct'){
    $reqProductId = isset($_POST['product_id'])?$_POST['product_id']:'';
    $reqMemberId = isset($_POST['member_id'])?$_POST['member_id']:'';
    $reqProductName = isset($_POST['product_name'])?$_POST['product_name']:'';
    $reqProductPrice = isset($_POST['product_price'])?$_POST['product_price']:'';
    $reqOrderNumber = isset($_POST['order_number'])?$_POST['order_number']:'';
    $reqOrderNote = isset($_POST['order_note'])?$_POST['order_note']:'';

    $attr = [
        'product_id'=> $reqProductId,
        'member_id'=> $reqMemberId,
        'product_name'=> $reqProductName,
        'product_price'=> $reqProductPrice,
        'order_number'=> $reqOrderNumber,
        'order_note'=> $reqOrderNote
    ];
    $res = $MBList->insertThis($attr);
    if($res['status']){

        $S_SCount = $MBList->countByMemberId($reqMemberId);
        $_SESSION['SCount'] = $S_SCount;

        $_SESSION['SESSION_STATUS']= 'SUCCESS';
        $_SESSION['SESSION_MESSAGE']= 'เพิ่มไปยังตะกร้าเรียบแล้ว';
    }
    else{
        $_SESSION['SESSION_STATUS']= 'ERROR';
        $_SESSION['SESSION_MESSAGE']= $res['message'];
    }

}

/* ========= page view ========= */
$SHOP = [];
$res = $MShop->selectThisId(1);
if($res['status']){
    $SHOP = $res['result'];
}
$GROUPS = [];
$res = $MPGroup->selectThisAll();
if($res['status']){
    $GROUPS = $res['result'];
}

$PRODUCTS = [];
$res = $MProduct->selectThisAll();
if($res['status']){
    $PRODUCTS = $res['result'];
    foreach ($PRODUCTS as $item){
        if($item['id']==$productId){
            $item['detail'] = str_replace("\n","<br>",$item['detail']);
            $PRODUCT = $item;
        }
    }
    if(!isset($PRODUCT['id'])){
        header( "location: /page-index.php" );
        exit(0);
    }
}
//echo json_encode($PRODUCTS);exit;

$UPLOADS = [];
$res = $MPUpload->selectThisAllProductId($productId);
if($res['status']){
    $UPLOADS = $res['result'];
}
//echo json_encode($UPLOADS);exit;

$COMMENTS = [];
$res = $MPComment->selectThisAllProductId($productId);
if($res['status']){
    $COMMENTS = $res['result'];
}
//echo json_encode($COMMENTS);exit;


/* ========= function ========= */
function dThai($timestamp){

    $month = ["","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
    $cutDT = explode(" ",$timestamp);
    $cut =  explode("-",$cutDT[0]);
    $d = $cut[2];
    $m = $cut[1];
    $y = $cut[0];
    $cut =  explode(":",$cutDT[1]);
    $h = $cut[0];
    $i = $cut[1];
    $resYMD = $d.' '. $month[intval($m)]. ' '. (intval($y)+543).' '.$h.':'.$i;

    return $resYMD;
}
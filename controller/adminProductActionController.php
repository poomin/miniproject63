<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 11:28 AM
 */


/* ========= require model ========= */
require_once __DIR__.'/../model/ProductGroupModel.php';
$MPGroup = new ProductGroupModel();
require_once __DIR__.'/../model/ProductModel.php';
$MProduct = new ProductModel();



/* ========= parameter set ========= */
$thisAlertStatus = '';  //E:error,S:success
$thisAlertText = '';

$ArrGroup = [];         // list product group

$thisProductId = isset($_REQUEST['id'])?$_REQUEST['id']:'-';
$thisProductGroupId = '';
$thisProductName = '';
$thisProductPrice = '';
$thisProductDetail = '';
$thisProductImg = 'img/df-upload.png';




/* ========= action method ========= */
$action = isset($_POST['action'])?$_POST['action']:'';
if($action == 'saveGroup'){

    $reqGroupName = isset($_POST['group_name'])?$_POST['group_name']:'';
    $attr=[
        'group_name'=> $reqGroupName,
        'this_remove'=> 'N',
    ];
    $res = $MPGroup->insertThis($attr);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];
    }

}
elseif($action == 'editGroup'){

    $reqGroupName = isset($_POST['group_name'])?$_POST['group_name']:'';
    $reqId = isset($_POST['id'])?$_POST['id']:'';

    $attr=[
        'group_name'=> $reqGroupName
    ];
    $res = $MPGroup->updateThis($attr,$reqId);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}
elseif($action == 'delGroup'){

    $reqId = isset($_POST['id'])?$_POST['id']:'';

    $res = $MPGroup->updateDeleteThis($reqId);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}


elseif($action == 'saveProduct'){

    $reqGroupId = isset($_POST['group_id'])?$_POST['group_id']:'';
    $reqName = isset($_POST['product_name'])?$_POST['product_name']:'';
    $reqPrice = isset($_POST['price'])?$_POST['price']:'';
    $reqImg = isset($_POST['img'])?$_POST['img']:'';
    $reqDetail = isset($_POST['detail'])?$_POST['detail']:'';

    $attr=[
        'group_id'=> $reqGroupId,
        'product_name'=> $reqName,
        'price'=> $reqPrice,
        'img'=> $reqImg,
        'detail'=> $reqDetail,
        'this_remove'=> 'N'
    ];
    $res = $MProduct->insertThis($attr);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
        header( "location: /admin-product-action.php?id=".$res['result'] );
        exit(0);
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];
    }

}
elseif($action == 'editProduct'){

    $reqGroupId = isset($_POST['group_id'])?$_POST['group_id']:'';
    $reqName = isset($_POST['product_name'])?$_POST['product_name']:'';
    $reqPrice = isset($_POST['price'])?$_POST['price']:'';
    $reqImg = isset($_POST['img'])?$_POST['img']:'';
    $reqDetail = isset($_POST['detail'])?$_POST['detail']:'';

    $attr=[
        'group_id'=> $reqGroupId,
        'product_name'=> $reqName,
        'price'=> $reqPrice,
        'img'=> $reqImg,
        'detail'=> $reqDetail,
        'this_remove'=> 'N'
    ];
    $res = $MProduct->updateThis($attr,$thisProductId);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];
    }

}

/* ========= page view ========= */
$ArrGroup = [];
$res = $MPGroup->selectThisAll();
if($res['status']){
    $ArrGroup = $res['result'];
}


$res = $MProduct->selectThisId($thisProductId);
if($res['status'] && isset($res['result']['id'])){
    $rObj = $res['result'];

    $thisProductGroupId = $rObj['group_id'];
    $thisProductName = $rObj['product_name'];
    $thisProductPrice = $rObj['price'];
    $thisProductDetail = $rObj['detail'];
    $thisProductImg = strlen($rObj['img'])>10?$rObj['img']:'img/df-upload.png';
}


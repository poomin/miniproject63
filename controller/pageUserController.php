<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 11:28 AM
 */

/* ========= require model ========= */
require_once __DIR__.'/../model/MemberModel.php';
require_once __DIR__.'/../model/ShopModel.php';
require_once __DIR__.'/../model/ProductGroupModel.php';
require_once __DIR__.'/../model/BillModel.php';
require_once __DIR__.'/../model/BillListModel.php';
require_once __DIR__.'/../model/BankModel.php';
$MMember = new MemberModel();
$MShop = new ShopModel();
$MPGroup = new ProductGroupModel();
$MBill = new BillModel();
$MBList = new BillListModel();
$MBank = new BankModel();


/* ========= parameter set ========= */
$thisAlertStatus = '';  //E:error,S:success
$thisAlertText = '';

//SESSION
$S_SID  = isset($S_SID)?$S_SID:'';
$S_SName  = isset($S_SName)?$S_SName:'';
$S_SType  = isset($S_SType)?$S_SType:'';
$S_SCount = isset($S_SCount)?$S_SCount:0;

$bankCode = [
    'bbl'=> 'ธนาคารกรุงเทพ',
    'bay'=> 'ธนาคารกรุงศรีอยุธยา',
    'kkb'=> 'ธนาคารกสิกรไทย',
    'ktb'=> 'ธนาคารกรุงไทย',
    'scb'=> 'ธนาคารไทยพาณิชย์',
];

$count_send = 0;

$memberName= '';
$memberPhone = '';
$memberAddress = '';

$BILLS = [];
$FINISH = [];




/* ========= action method ========= */
$thisPage = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$thisStep = isset($_REQUEST['step'])?$_REQUEST['step']:1;
$thisBankId= isset($_REQUEST['bank'])?$_REQUEST['bank']:1;
$thisSend= isset($_REQUEST['send'])?$_REQUEST['send']:30;


$action = isset($_POST['action'])?$_POST['action']:'';
if($action == 'delProduct'){

    $reqId = isset($_POST['id'])?$_POST['id']:'';

    $res = $MBList->deleteThis($reqId);
    if($res['status']){
        $S_SCount = $MBList->countByMemberId($S_SID);
        $_SESSION['SCount'] = $S_SCount;

        $thisAlertStatus='S';
        $thisAlertText='ลบสินค้าในตะกร้าสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}

elseif ($action=='payment'){

    $reqMemberName = isset($_POST['member_name'])?$_POST['member_name']:'';
    $reqMemberTel = isset($_POST['member_tel'])?$_POST['member_tel']:'';
    $reqMemberAddress = isset($_POST['member_address'])?$_POST['member_address']:'';
    $reqBank = isset($_POST['bank'])?$_POST['bank']:'';
    $reqSend = isset($_POST['send'])?$_POST['send']:'';
    $reqSlip = isset($_POST['slip'])?$_POST['slip']:'';

    $billPrice = 0;
    $billDate = date("Y-m-d");
    $billNote = '';
    $res= $MBList->selectCartByMemberId($S_SID);
    if($res['status']){
        foreach ($res['result'] as $item){
            $billPrice+= ($item['product_price']*$item['order_number']);
        }
    }

    $shippingName = '';
    $shippingPrice = '';
    if($reqSend==30){
        $shippingName = 'จัดส่งแบบลงทะเบียน';
        $shippingPrice = 30;
    }
    elseif ($reqSend==50){
        $shippingName = 'จัดส่งแบบ EMS';
        $shippingPrice = 50;
    }

    $bankCode = '';
    $bankNumber= '';
    $res = $MBank->selectThisId($reqBank);
    if($res['status'] && isset($res['result'])){
        $bankCode = $res['result']['bank_code'];
        $bankNumber = $res['result']['bank_number'];
    }


    $attr=[
        'member_id'=> $S_SID,
        'member_name'=> $reqMemberName,
        'member_address'=> $reqMemberAddress,
        'member_tel'=> $reqMemberTel,
        'bill_type'=> 'B',
        'bill_price'=> $billPrice,
        'bill_date'=> $billDate,
        'bill_note'=> $billNote,
        'shipping_name'=> $shippingName,
        'shipping_price'=> $shippingPrice,
        'bank_code'=> $bankCode,
        'bank_number'=> $bankNumber,
        'slip_img'=> $reqSlip
    ];
    $res = $MBill->insertThis($attr);
    if($res['status']){
        $S_SCount = 0;
        $_SESSION['SCount'] = $S_SCount;

        $bId = $res['result'];
        $r = $MBList->updateThisCartByMemberId($S_SID,$bId);

        $thisAlertStatus='S';
        $thisAlertText='ชำระสินค้าสำเร็จ <br>รอการตรวจสอบการชำระจากแอดมิน';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }


}

elseif ($action=='confirm'){

    $reqBillId = isset($_POST['bill_id'])?$_POST['bill_id']:'';
    $attr=[
        'bill_type'=> 'E',
        'bill_note'=> 'ได้รับสินค้าเรียบร้อยแล้ว'
    ];
    $res = $MBill->updateStatus($attr,$reqBillId);
    if($res['status']){

        $thisAlertStatus='S';
        $thisAlertText='ยืนยันได้รับสินค้าเรียบร้อยแล้ว';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }


}

/* ========= page view ========= */
//member
$res = $MMember->selectThisId($S_SID);
if($res['status'] && isset($res['result']['id'])){
    $memberName= $res['result']['member_name'];
    $memberPhone = $res['result']['tel'];
    $memberAddress = $res['result']['address'];
}

$SHOP = [];
$res = $MShop->selectThisId(1);
if($res['status']){
    $SHOP = $res['result'];
}
$GROUPS = [];
$res = $MPGroup->selectThisAll();
if($res['status']){
    $GROUPS = $res['result'];
}


$VIEW_PAGE1 = [];
$sumPage1 = 0;
if($thisPage==1){
    $res = $MBList->selectCartByMemberId($S_SID);
    if($res['status']){
        $VIEW_PAGE1 = $res['result'];
    }
}

$BANKS = [];
$res = $MBank->selectThis();
if($res['status']){
    $result = $res['result'];
    foreach ($result as $item){
        $item['bank_name'] = isset($bankCode[$item['bank_code']])?$bankCode[$item['bank_code']]:'';
        $BANKS[] = $item;
    }
}


$BILLS = [];
$FINISH = [];
$res = $MBill->selectThisAllMemberId($S_SID);
if($res['status']){
    foreach ($res['result'] as $item){
        if($item['bill_type'] == 'C' || $item['bill_type']== 'E'){
            $FINISH[] = $item;
        }
        elseif ($item['bill_type'] != 'E'){
            $BILLS[] = $item;
        }
    }

    $count_send = count($BILLS);
}

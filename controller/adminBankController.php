<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 11:28 AM
 */


/* ========= require model ========= */
require_once __DIR__.'/../model/BankModel.php';
$MBank = new BankModel();
$bankCode = [
    'bbl'=> 'ธนาคารกรุงเทพ',
    'bay'=> 'ธนาคารกรุงศรีอยุธยา',
    'kkb'=> 'ธนาคารกสิกรไทย',
    'ktb'=> 'ธนาคารกรุงไทย',
    'scb'=> 'ธนาคารไทยพาณิชย์',
];


/* ========= parameter set ========= */
$thisAlertStatus = '';  //E:error,S:success
$thisAlertText = '';

$BANKS = [];


/* ========= action method ========= */

$action = isset($_POST['action'])?$_POST['action']:'';
if($action == 'insert'){

    $reqBankCode = isset($_POST['bank_code'])?$_POST['bank_code']:'';
    $reqBankNumber = isset($_POST['bank_number'])?$_POST['bank_number']:'';
    $reqBankAcc = isset($_POST['bank_account'])?$_POST['bank_account']:'';

    $attr=[
        'bank_code'=> $reqBankCode,
        'bank_number'=> $reqBankNumber,
        'bank_account'=> $reqBankAcc,
    ];
    $res = $MBank->insertThis($attr);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];
    }

}
elseif($action == 'update'){

    $reqBankCode = isset($_POST['bank_code'])?$_POST['bank_code']:'';
    $reqBankNumber = isset($_POST['bank_number'])?$_POST['bank_number']:'';
    $reqBankAcc = isset($_POST['bank_account'])?$_POST['bank_account']:'';

    $reqBankId= isset($_POST['bank_id'])?$_POST['bank_id']:'';

    $attr=[
        'bank_code'=> $reqBankCode,
        'bank_number'=> $reqBankNumber,
        'bank_account'=> $reqBankAcc,
    ];
    $res = $MBank->updateThis($attr,$reqBankId);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='แก้ไขข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];
    }

}
elseif($action == 'delete'){

    $reqBankId= isset($_POST['bank_id'])?$_POST['bank_id']:'';
    $res = $MBank->deleteThis($reqBankId);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='ลบข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];
    }

}



/* ========= page view ========= */
$BANKS = [];
$res = $MBank->selectThis();
if($res['status']){
    $result = $res['result'];
    foreach ($result as $item){
        $item['bank_name'] = isset($bankCode[$item['bank_code']])?$bankCode[$item['bank_code']]:'';
        $BANKS[] = $item;
    }
}
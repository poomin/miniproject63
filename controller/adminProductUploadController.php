<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/18/2021
 * Time: 12:27 PM
 */

/* ========= require model ========= */
require_once __DIR__.'/../model/ProductModel.php';
$MProduct = new ProductModel();
require_once __DIR__.'/../model/ProductUploadModel.php';
$MPUpload = new ProductUploadModel();




/* ========= parameter set ========= */
$alertStatus = false;
$alertType = '';// S:success,W:warn,D:danger
$alertText = '';

$productId = '0';
$productName = '';
$productImg = '';
$productPrice = 0;
$productDetail = '';

$UPLOADS = [];





/* ========= action method ========= */
$productId = isset($_REQUEST['id'])?$_REQUEST['id']: $productId;

$action = isset($_POST['action'])?$_POST['action']:'';
if($action =='uploadImg'){
    $reqImg = isset($_POST['path'])?$_POST['path']:'';

    $attr=[
        'product_id'=> $productId,
        'img'=> $reqImg,
    ];
    $res = $MPUpload->insertThis($attr);
    if($res['status']){
        $alertStatus = true;
        $alertType='S';
        $alertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $alertStatus = true;
        $alertType='E';
        $alertText=$res['message'];
    }
}





/* ========= page view ========= */
$res = $MProduct->selectThisId($productId);
if($res['status'] && isset($res['result']['id'])){
    $result = $res['result'];
    $productName = $result['product_name'];
    $productPrice = $result['price'];
    $productDetail = $result['detail'];
    $productImg = $result['img'];

    $productDetail = str_replace("\n","<br>",$productDetail);
}
else{
    header( "location: admin-product.php" );
    exit(0);
}


$UPLOADS = [];
$res = $MPUpload->selectThisAllProductId($productId);
if($res['status']){
    $UPLOADS = $res['result'];
}

<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 11:28 AM
 */


/* ========= require model ========= */
require_once __DIR__.'/../model/ShopModel.php';
$MShop = new ShopModel();



/* ========= parameter set ========= */
$thisAlertStatus = '';  //E:error,S:success
$thisAlertText = '';

$thisShopName = '';
$thisShopOwner = '';
$thisShopPhone = '';

$thisShopFbName = '';
$thisShopFbLink = '';

$thisShopLineName = '';
$thisShopLineLink = '';

$thisShopAddress = '';

$thisShopMap = '';

$thisShopQrCode = 'img/df-upload.png';



/* ========= action method ========= */
$action = isset($_POST['action'])?$_POST['action']:'';
if($action == 'saveShop'){

    $reqShopName = isset($_POST['shop_name'])?$_POST['shop_name']:'';
    $reqShopOwner = isset($_POST['shop_owner'])?$_POST['shop_owner']:'';
    $reqTel = isset($_POST['tel'])?$_POST['tel']:'';

    $attr=[
        'id'=> 1,
        'shop_name'=> $reqShopName,
        'shop_owner'=> $reqShopOwner,
        'tel'=> $reqTel,
    ];
    $res = $MShop->insertUpdateShop($attr);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}
elseif($action == 'saveFb'){

    $reqShopFbName = isset($_POST['fb_name'])?$_POST['fb_name']:'';
    $reqShopFbLink = isset($_POST['fb_link'])?$_POST['fb_link']:'';

    $attr=[
        'id'=> 1,
        'fb_name'=> $reqShopFbName,
        'fb_link'=> $reqShopFbLink,
    ];
    $res = $MShop->insertUpdateFb($attr);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}
elseif($action == 'saveLine'){

    $reqShopLineName = isset($_POST['line_name'])?$_POST['line_name']:'';
    $reqShopLineLink = isset($_POST['line_link'])?$_POST['line_link']:'';

    $attr=[
        'id'=> 1,
        'line_name'=> $reqShopLineName,
        'line_link'=> $reqShopLineLink,
    ];
    $res = $MShop->insertUpdateLine($attr);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}
elseif($action == 'saveAddress'){

    $reqShopAddress = isset($_POST['address'])?$_POST['address']:'';

    $attr=[
        'id'=> 1,
        'address'=> $reqShopAddress,
    ];
    $res = $MShop->insertUpdateAddress($attr);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}
elseif($action == 'saveMap'){

    $reqShopGoogleMap = isset($_POST['google_map'])?$_POST['google_map']:'';
    $cutSrc = explode("src",$reqShopGoogleMap);
    if(count($cutSrc)>=2){
        $textSrc = $cutSrc[1];
        $cutComma = explode('"',$textSrc);
        if(count($cutComma)>=2){
            $reqShopGoogleMap = $cutComma[1];
        }
    }


    $attr=[
        'id'=> 1,
        'google_map'=> $reqShopGoogleMap,
    ];
    $res = $MShop->insertUpdateMap($attr);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}
elseif($action == 'saveImg'){

    $reqShopQrCode = isset($_POST['src'])?$_POST['src']:'';

    $attr=[
        'id'=> 1,
        'line_qrcode'=> $reqShopQrCode,
    ];
    $res = $MShop->insertUpdateQrCode($attr);
    if($res['status']){
        $thisAlertStatus='S';
        $thisAlertText='บันทึกข้อมูลสำเร็จ';
    }
    else{
        $thisAlertStatus='E';
        $thisAlertText=$res['message'];

    }

}



/* ========= page view ========= */
$res = $MShop->selectThisId(1);
if($res['status'] && isset($res['result']['id'])){
    $rObj = $res['result'];

    $thisShopName = $rObj['shop_name'];
    $thisShopOwner = $rObj['shop_owner'];
    $thisShopPhone = $rObj['tel'];

    $thisShopFbName = $rObj['fb_name'];
    $thisShopFbLink = $rObj['fb_link'];

    $thisShopLineName = $rObj['line_name'];
    $thisShopLineLink = $rObj['line_link'];

    $thisShopAddress = $rObj['address'];

    $thisShopMap= $rObj['google_map'];

    $thisShopQrCode = strlen($rObj['line_qrcode'])>10?$rObj['line_qrcode']:$thisShopQrCode;

}


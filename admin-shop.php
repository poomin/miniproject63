<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 12:31 PM
 */
$MENU = 'shop';
require_once __DIR__.'/controller/adminShopController.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_adminHeader.php';?>

</head>
<body>

<!-- =========== Start: Top Menu =========== -->
<div class="sticky-top">
    <?php require_once __DIR__.'/_adminMenu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="pl-2 pr-2">
    <div class="container-fluid">
        <div class="page-content shadow mt-3 mb-5 p-3">

            <!-- header -->
            <div class="mb-3 border-bottom pb-1">
                <h5><i class="fas fa-store"></i> ร้านค้า</h5>
            </div>

            <!-- alert message -->
            <?php if($thisAlertStatus=='S'):?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong class="alert-heading">สำเร็จ</strong>
                <span class="d-block"><?php echo $thisAlertText;?></span>
            </div>
            <?php elseif ($thisAlertStatus=='E'):?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong class="alert-heading">แจ้งเตือน</strong>
                <span class="d-block"><?php echo $thisAlertText;?></span>
            </div>
            <?php endif;?>

            <!-- form post -->
            <div class="container">

                <span class="d-block">ข้อมูลร้านค้า</span>
                <form class="row g-3" method="post">
                    <div class="col">
                        <label for="inputShopName" class="visually-hidden">Shop name</label>
                        <input type="text" class="form-control" id="inputShopName" placeholder="ชื่อร้านค้า" name="shop_name" value="<?php echo $thisShopName;?>" required>
                    </div>
                    <div class="col">
                        <label for="inputShopOwner" class="visually-hidden">Shop owner</label>
                        <input type="text" class="form-control" id="inputShopOwner" placeholder="เจ้าของร้านค้า" name="shop_owner" value="<?php echo $thisShopOwner;?>" required>
                    </div>
                    <div class="col">
                        <label for="inputShopPhone" class="visually-hidden">Shop phone</label>
                        <input type="text" class="form-control" id="inputShopPhone" placeholder="เบอร์โทรติดต่อร้านค้า" name="tel" value="<?php echo $thisShopPhone;?>" required>
                    </div>
                    <div class="col-auto">
                        <input class="d-none" type="text" name="action" value="saveShop" placeholder="">
                        <button type="submit" class="btn btn-outline-success mb-3"><i class="fas fa-save"></i> บันทึก</button>
                    </div>
                </form>

                <span class="d-block">Facebook</span>
                <form class="row g-3" method="post">
                    <div class="col">
                        <label for="inputShopFbName" class="visually-hidden">Facebook name</label>
                        <input type="text" class="form-control" id="inputShopFbName" placeholder="ชื่อ Facebook" name="fb_name" value="<?php echo $thisShopFbName;?>" required>
                    </div>
                    <div class="col">
                        <label for="inputShopFbLink" class="visually-hidden">Facebook link</label>
                        <input type="text" class="form-control" id="inputShopFbLink" placeholder="ลิงค์ Facebook" name="fb_link" value="<?php echo $thisShopFbLink;?>"  required>
                    </div>
                    <div class="col-auto">
                        <input class="d-none" type="text" name="action" value="saveFb" placeholder="">
                        <button type="submit" class="btn btn-outline-success mb-3"><i class="fas fa-save"></i> บันทึก</button>
                    </div>
                </form>

                <span class="d-block">Line</span>
                <form class="row g-3" method="post">
                    <div class="col">
                        <label for="inputShopLineName" class="visually-hidden">Line name</label>
                        <input type="text" class="form-control" id="inputShopLineName" placeholder="ชื่อ Line" name="line_name" value="<?php echo $thisShopLineName;?>" required>
                    </div>
                    <div class="col">
                        <label for="inputShopLineLink" class="visually-hidden">Line link</label>
                        <input type="text" class="form-control" id="inputShopLineLink" placeholder="ลิงค์ Line" name="line_link" value="<?php echo $thisShopLineLink;?>" required>
                    </div>
                    <div class="col-auto">
                        <input class="d-none" type="text" name="action" value="saveLine" placeholder="">
                        <button type="submit" class="btn btn-outline-success mb-3"><i class="fas fa-save"></i> บันทึก</button>
                    </div>
                </form>

                <div class="mb-5">
                    <div class="mt-1">
                        <div class="text-center mt-2">
                            <label class="p-2 text-center" for="imageUploadId" style="cursor: pointer; max-width: 300px; border:2px dashed #0d6efd;">
                                <img id="pathImageUploadId" class="img-fluid w-75" src="<?php echo $thisShopQrCode;?>">
                                <input class="d-none" id="imageUploadId" accept="image/*" type="file" onchange="imageUpload(this)">
                            </label>
                        </div>
                        <div class="text-center small text-muted">
                            Line QR Code Upload
                        </div>
                        <div class="row align-items-center d-none" id="imageUploadProgressbarId">
                            <div class="col">
                                <div class="progress">
                                    <div id="progressbarId" class="progress-bar" style="width: 1%;" role="progressbar"></div>
                                </div>
                            </div>
                            <div class="col-auto pl-0">
                                <span id="cancelUploadImageId" class="badge bg-danger" style="cursor: pointer;" onclick="cancelUpload()">
                                    <i class="fas fa-times"></i>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>

                <span class="d-block">ที่ตั้ง</span>
                <form class="row g-3" method="post">
                    <div class="col">
                        <label for="inputShopAddress" class="visually-hidden"> Address </label>
                        <input type="text" class="form-control" id="inputShopAddress" placeholder="ที่ตั้งร้าน" name="address" value="<?php echo $thisShopAddress;?>" required>
                    </div>
                    <div class="col-auto">
                        <input class="d-none" type="text" name="action" value="saveAddress" placeholder="">
                        <button type="submit" class="btn btn-outline-success mb-3"><i class="fas fa-save"></i> บันทึก</button>
                    </div>
                </form>

                <span class="d-block">Google map</span>
                <form class="row g-3" method="post">
                    <div class="col">
                        <label for="inputShopMap" class="visually-hidden"> map </label>
                        <input type="text" class="form-control" id="inputShopMap" placeholder="google map" name="google_map" value="<?php echo $thisShopMap;?>" required>
                    </div>
                    <div class="col-auto">
                        <input class="d-none" type="text" name="action" value="saveMap" placeholder="">
                        <button type="submit" class="btn btn-outline-success mb-3"><i class="fas fa-save"></i> บันทึก</button>
                    </div>
                </form>
                <?php if(strlen($thisShopMap)>50):?>
                <div class="mb-5 p-3 border shadow">
                    <!--Google map-->
                    <iframe src="<?php echo $thisShopMap;?>" frameborder="0" style="border:0;height:300px;width:100%;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    <!--Google Maps-->
                </div>
                <?php endif;?>

            </div>

        </div>
    </div>

</div>
<!-- ____________ End: Body ____________ -->


<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_adminScript.php';?>
<!-- ____________ End: Script ____________ -->


<!-- =========== Start: Custom Script =========== -->

<script>

    //upload image
    let ajax_img;
    function imageUpload(input) {
        if (input.files && input.files[0]) {
            ajax_img = new XMLHttpRequest();

            $('#imageUploadProgressbarId').removeClass('d-none');

            let form_data = new FormData();
            form_data.append("fileToUpload", input.files[0]);
            ajax_img.upload.addEventListener("progress", progressHandler, false);
            ajax_img.addEventListener("load", completeHandler, false);
            ajax_img.addEventListener("error", errorHandler, false);
            ajax_img.addEventListener("abort", abortHandler, false);
            ajax_img.open("POST","/upload/upload_file.php");
            ajax_img.send(form_data);

            function progressHandler(event) {
                let percent = (event.loaded / event.total) * 100;
                $("#progressbarId").css('width', Math.round(percent) + "%");
            }

            function completeHandler(event) {
                let data_return = JSON.parse(event.target.responseText);
                if (data_return['status'] === 'ok') {
                    let src = '/upload/uploads/'+data_return['new_name'];

                    $("#progressbarId").css('width', "0%");
                    $("#imageUploadProgressbarId").addClass("d-none");

                    $("#pathImageUploadId").attr("src",src);
                    saveImg(src);

                } else {
                    ajax_img.abort();
                    $("#progressbarId").css('width', "0%");
                    $("#imageUploadProgressbarId").addClass("d-none");
                    alert("Error:" + data_return['message']);
                }
            }

            function errorHandler(event) {
                ajax_img.abort();
                $("#progressbarId").css('width', "0%");
                $("#imageUploadProgressbarId").addClass("d-none");
                alert("Upload Failed");
            }

            function abortHandler(event) {
                ajax_img.abort();
                $("#progressbarId").css('width', "0%");
                $("#imageUploadProgressbarId").addClass("d-none");
                alert("Upload Aborted");
            }
        }
        else {
            alert("Not found file input!!!");
        }
    }
    function cancelUpload() {
        if(ajax_img!=null){
            ajax_img.abort();
        }
        $("#progressbarId").css('width', "0%");
        $("#imageUploadProgressbarId").addClass("d-none");
        alert("Cancel Upload Image");
    }
    function saveImg(src) {
        let form = $(document.createElement('form'));
        $(form).attr("method","POST");
        $(form).attr('hidden',true);
        let input_src = $("<input>")
            .attr("type","text")
            .attr("name","src")
            .attr("value",src);
        $(form).append($(input_src));
        let input_action = $("<input>")
            .attr("type","text")
            .attr("name","action")
            .attr("value","saveImg");
        $(form).append($(input_action));

        form.appendTo( document.body );
        $(form).submit();
    }

</script>
<!-- ____________ End: Custom Script ____________ -->


</body>

</html>

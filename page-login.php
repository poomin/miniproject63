<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 10:42 AM
 */
require_once __DIR__.'/_session.php';
require_once __DIR__.'/controller/pageLoginController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_header.php';?>

</head>
<body>


<!-- =========== Start: Top Menu =========== -->
<div class="container fixed-top">
    <?php require_once __DIR__.'/_menu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="container p-body">

    <!-- login -->
    <div class="p-2 mt-5" id="formLoginId">
        <div class="mt-5">
            <div class="shadow p-3 mx-auto bg-white bg-unlock" style="max-width: 500px;">

                <div class="mt-3 text-center">
                    <h5>เข้าสู่ระบบ</h5>
                </div>

                <!-- แจ้งเตือน -->
                <?php if($alertStatus):?>
                    <div class="alert alert-warning alert-dismissible fade show shadow" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        <strong class="alert-heading">แจ้งเตือน</strong>
                        <span class="d-block"> <?php echo $alertMessage;?> </span>
                    </div>
                <?php endif;?>

                <form class="mb-3" method="post">
                    <div class="mb-3">
                        <label class="t-r" for="usernameId">Username</label>
                        <input id="usernameId" class="form-control" type="text" name="username" required>
                    </div>
                    <div class="mb-3">
                        <label class="t-r" for="passwordId">Password</label>
                        <input id="passwordId" class="form-control" type="password" name="password" required>
                    </div>
                    <div class="text-center mt-2">
                        <input class="d-none" type="text" name="action" value="login" placeholder="">
                        <button class="btn btn-outline-success" type="submit">เข้าสู่ระบบ</button>
                    </div>

                    <div class="text-center mt-5">
                        <button class="btn btn-outline-primary btn-sm" type="button" onclick="showForm('U')">
                            <i class="fas fa-user"></i> สมัครสมาชิก
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- register -->
    <div class="p-2 mt-5 d-none" id="formRegisterId">
        <div class="mt-5">
            <div class="shadow p-3 mx-auto bg-white bg-unlock" style="max-width: 500px;">
                <div class="mt-3 text-center">
                    <h5>สมัครสมาชิก</h5>
                </div>
                <form class="mb-2" method="post">

                    <div class="mb-3">
                        <label class="t-r" for="inputUsernameId">Username </label>
                        <input id="inputUsernameId" class="form-control" type="text" name="username" required>
                    </div>

                    <div class="row g-3 mb-3">
                        <div class="col-md-6">
                            <label class="t-r" for="inputPasswordId">Password </label>
                            <input id="inputPasswordId" class="form-control" type="password" name="password" required>
                        </div>
                        <div class="col-md-6">
                            <label class="t-r" for="confirmPasswordId">Confirm password </label>
                            <input id="confirmPasswordId" class="form-control" type="password" required>
                            <small id="mAddTextError" class="form-text text-danger d-none">Confirm password not same !!! </small>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label class="t-r" for="inputNameId">ชื่อ-สกุล </label>
                        <input id="inputNameId" class="form-control" type="text" name="member_name" required>
                    </div>
                    <div class="mb-3">
                        <label class="t-r" for="inputPhoneId"> เบอร์โทร </label>
                        <input id="inputPhoneId" class="form-control" type="text" name="tel" required>
                    </div>
                    <div class="mb-3">
                        <label class="t-r" for="inputAddressId"> ที่อยู่ </label>
                        <textarea id="inputAddressId" class="form-control" name="address" required></textarea>
                    </div>

                    <div class="text-center mt-2">
                        <input class="d-none" type="text" name="action" value="addMember" placeholder="">
                        <button class="btn btn-outline-success" type="submit">สมัครสมาชิก</button>
                    </div>

                    <div class="text-center mt-5">

                        <button class="btn btn-outline-info btn-sm" type="button" onclick="showForm('L')">
                            <i class="fas fa-lock"></i> เข้าสู่ระบบ
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>
<!-- ____________ End: Body ____________ -->


<!-- =========== Start: Footer =========== -->
<div class="container-fluid footer pt-4 pb-4 bg-8">
    <?php require_once __DIR__.'/_footer.php';?>
</div>
<!-- ____________ End: Footer ____________ -->


<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_script.php';?>
<!-- ____________ End: Script ____________ -->

<!-- modal product shop -->
<?php require_once __DIR__.'/_modalProduct.php';?>


<!-- =========== Start: Custom Script =========== -->
<script>

    //click show form  (L:login,R:register)
    function showForm(type) {
        if(type==="L"){
            $("#formLoginId").removeClass("d-none");
            $("#formRegisterId").addClass("d-none");
        }
        else {
            $("#formLoginId").addClass("d-none");
            $("#formRegisterId").removeClass("d-none");
        }
    }


    //check confirm password
    $("#confirmPasswordId").on("change",function () {
        let pass =  $("#inputPasswordId").val();
        let conf = $("#confirmPasswordId").val();
        if(pass === conf){
            $("#mAddTextError").addClass("d-none");
        }else{
            $("#mAddTextError").removeClass("d-none");
            $("#confirmPasswordId").val("");
        }
    });

</script>
<!-- ____________ End: Custom Script ____________ -->


</body>
</html>

<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/25/2021
 * Time: 11:37 AM
 */

//SESSION
$S_SID  = isset($S_SID)?$S_SID:'';
$CART_PRODUCTS = [];
$CART_SUM = 0;
if($S_SID!=''){
    require_once __DIR__.'/model/BillListModel.php';
    $CPBillList = new BillListModel();
    $res = $CPBillList->selectCartByMemberId($S_SID);
    if($res['status']){
        $CART_PRODUCTS = $res['result'];
    }
}





$modal_status_df = '-';
$modal_message_df = '-';
$modal_status_df = isset($_SESSION['SESSION_STATUS'])?$_SESSION['SESSION_STATUS']:$modal_status_df;
$modal_message_df = isset($_SESSION['SESSION_MESSAGE'])?$_SESSION['SESSION_MESSAGE']:$modal_message_df;
if($modal_status_df!='-'){
    unset($_SESSION['SESSION_STATUS']);
    unset($_SESSION['SESSION_MESSAGE']);
}


$modal_show = false;
$modal_status = isset($modal_status)?$modal_status:$modal_status_df;
$modal_header = '';
$modal_class = '';
$modal_message = isset($modal_message)?$modal_message:$modal_message_df;
$modal_icon = '';
if(strtoupper($modal_status)=='SUCCESS'){
    $modal_header = 'Success';
    $modal_show = true;
    $modal_class = ' text-success ';
    $modal_icon = 'fas fa-check-circle';
}elseif(strtoupper($modal_status)=='WARN'){
    $modal_header = 'Warning';
    $modal_show = true;
    $modal_class = ' text-warning ';
    $modal_icon = 'fas fa-exclamation-triangle';
}elseif(strtoupper($modal_status)=='ERROR'){
    $modal_header = 'Error';
    $modal_show = true;
    $modal_class = ' text-danger ';
    $modal_icon = 'fas fa-ban';
}

?>

<!-- modal message -->
<div class="modal fade" id="modalMessageAlert" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalMessageAlertLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalMessageAlertLabel">แจ้งเตือน</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="text-center mb-3">
                    <i class="<?php echo $modal_icon;?> fa-4x <?php echo $modal_class;?>"></i>
                </div>
                <div class="text-center <?php echo $modal_class;?>">
                    <strong><?php echo $modal_header;?></strong>
                    <p><?php echo $modal_message;?></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- modal cart product -->
<div class="modal fade" id="modalCartProduct" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalCartProductLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalCartProductLabel">รายการในตะกร้า</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <tbody>
                        <?php foreach ($CART_PRODUCTS as $key=>$item):
                            $CART_SUM += ($item['order_number']*$item['product_price']);
                            ?>
                        <tr class="p-2">
                            <td><?php echo ($key+1);?></td>
                            <td>
                                <img class="img-fluid rounded" style="max-height: 60px;" src="<?php echo $item['img'];?>">
                            </td>
                            <td>
                                <div>
                                    <strong class="fw-bold text-primary"><?php echo $item['product_name'];?></strong>
                                </div>
                                <div>
                                    ราคา <span class="badge bg-danger"><?php echo $item['product_price'];?></span> บาท
                                </div>
                                <div>
                                    <small class="text-muted"><?php echo $item['order_note'];?></small>
                                </div>
                                <div class="clearfix">
                                    <div class="float-start">
                                        จำนวน <strong> <?php echo $item['order_number'];?> </strong>
                                    </div>
                                    <div class="float-end">
                                        รวม <span class="badge bg-success"><?php echo number_format($item['order_number']*$item['product_price'],2,'.',',')?></span> บาท
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                    <div class="text-end">
                        รวม <strong class="pe-2 ps-2 fw-bold text-success"><?php echo number_format($CART_SUM,2,'.',',');?></strong> บาท
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>

    function modalAlertMessageShow() {
        let myModalAlertEl = document.getElementById('modalMessageAlert');
        let myModalAlert = new bootstrap.Modal(myModalAlertEl);
        myModalAlert.show();
    }
</script>

<?php if($modal_show):?>
<script>
    modalAlertMessageShow();
</script>
<?php endif;?>


<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 12:31 PM
 */
$MENU = 'product';
require_once __DIR__.'/controller/adminProductController.php'
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_adminHeader.php';?>

    <!-- dataTable bootstrap 5 -->
    <link rel="stylesheet" href="lib/dataTable/css/dataTables.bootstrap5.min.css">

</head>
<body>

<!-- =========== Start: Top Menu =========== -->
<div class="sticky-top">
    <?php require_once __DIR__.'/_adminMenu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="pl-2 pr-2">
    <div class="container-fluid">
        <div class="page-content shadow mt-3 mb-5 p-3">

            <!-- header -->
            <div class="mb-3 border-bottom clearfix pb-1">
                <div class="float-start">
                    <h5 class="float-left"><i class="fas fa-gifts"></i> สินค้า</h5>
                </div>
                <div class="float-end">
                    <a class="btn btn-outline-success btn-sm" href="admin-product-action.php">
                        <i class="fas fa-plus-square"></i>
                    </a>
                </div>
            </div>

            <!-- alert message -->
            <?php if($thisAlertStatus=='S'):?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <strong class="alert-heading">สำเร็จ</strong>
                    <span class="d-block"><?php echo $thisAlertText;?></span>
                </div>
            <?php elseif ($thisAlertStatus=='E'):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <strong class="alert-heading">แจ้งเตือน</strong>
                    <span class="d-block"><?php echo $thisAlertText;?></span>
                </div>
            <?php endif;?>

            <!-- table list -->
            <div class="container">
                <div class="table-responsive mb-5">
                    <table class="dataTableNone table table-sm text-nowrap w-100">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ภาพ</th>
                            <th>ประเภท</th>
                            <th>สินค้า</th>
                            <th>ราคา</th>
                            <th>จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($ArrProduct as $key=>$item):?>
                        <tr>
                            <td><?php echo $key+1;?></td>
                            <td>
                                <img class="img-fluid rounded" src="<?php echo $item['img'];?>" style="max-width: 150px; max-height: 150px;">
                            </td>
                            <td> <?php echo $item['group_name'];?> </td>
                            <td> <?php echo $item['product_name'];?> </td>
                            <td> <?php echo number_format($item['price']);?> บาท </td>
                            <td>
                                <a class="btn btn-outline-primary btn-sm" href="admin-product-upload.php?id=<?php echo $item['id'];?>">
                                    <i class="fas fa-images"></i>
                                </a>
                                <a class="btn btn-outline-warning btn-sm" href="admin-product-action.php?id=<?php echo $item['id'];?>">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <button type="button" class="btn btn-outline-danger btn-sm" onclick="modalDeleteProductShow('<?php echo $item['id'];?>','<?php echo $item['product_name'];?>')">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

</div>
<!-- ____________ End: Body ____________ -->


<!-- =========== Start: Modal =========== -->
<!-- modal delete -->
<div class="modal fade" id="modalDelete" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content" method="post">
            <div class="modal-header">
                <h5 class="modal-title text-danger" id="modalDeleteLabel">ลบข้อมูล</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="text-center mb-3">
                    <i class="fas fa-ban fa-4x text-danger"></i>
                </div>
                <div class="text-center text-danger">
                    <strong>Delete</strong>
                    <p id="mDelText">ลบข้อมูล [สินค้า]</p>
                </div>
            </div>
            <div class="modal-footer">
                <input id="mDelId" class="d-none" type="text" name="id" value="" placeholder="">
                <input class="d-none" type="text" name="action" value="delProduct" placeholder="">

                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>
    </div>
</div>
<!-- ____________ End: Modal ____________ -->



<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_adminScript.php';?>
<!-- ____________ End: Script ____________ -->


<!-- =========== Start: Custom Script =========== -->
<!-- css dataTable bootstrap 5-->
<script src="lib/dataTable/js/jquery.dataTables.min.js"></script>
<script src="lib/dataTable/js/dataTables.bootstrap5.min.js"></script>
<script src="lib/dataTable/js/dataTable.js"></script>

<script>

    //delete product
    function modalDeleteProductShow(id,text) {
        $("#modalDelete").modal("show");

        $("#mDelText").html("ลบข้อมูล "+text);
        $("#mDelId").val(id);
    }


</script>
<!-- ____________ End: Custom Script ____________ -->


</body>

</html>

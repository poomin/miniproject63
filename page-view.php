<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/18/2021
 * Time: 9:56 AM
 */
require_once __DIR__.'/_session.php';
require_once __DIR__.'/controller/pageViewController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_header.php';?>

</head>
<body>


<!-- =========== Start: Top Menu =========== -->
<div class="container fixed-top">
    <?php require_once __DIR__.'/_menu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="container p-body">

    <!-- top sell -->
    <div class="row shadow bg-white p-2 rounded">
        <div class="col-md-3">
            <img class="img-fluid rounded" src="<?php echo $PRODUCT['img'];?>">
        </div>
        <div class="col-md-5 ps-5">
            <h5 class="text-primary"><i class="fas fa-cookie-bite"></i> <?php echo $PRODUCT['product_name'];?> </h5>
            <div>
                <span class="fw-bold badge bg-danger" style="font-size: 18px;"> <?php echo $PRODUCT['price'];?> -</span>
            </div>
            <div class="mt-2">
                <small class="text-secondary border-bottom pb-1 d-block"><i class="fas fa-edit mr-2"></i>รายละเอียด</small>
                <div class="mt-1">
                    <?php echo $PRODUCT['detail'];?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <label for="inputNumberId" class="small text-muted me-2 pt-1"> จำนวน </label>
            <div class="input-group mb-3">
                <button class="btn btn-outline-secondary btn-sm" type="button" onclick="fnReduce()">-</button>
                <input id="inputNumberId" type="number" class="form-control form-control-sm text-center" style="width: 100px!important;flex: 0 0 auto;" value="1">
                <button class="btn btn-outline-secondary btn-sm" type="button" onclick="fnAdd()">+</button>
            </div>
            <div class="mt-1">
                <?php if($S_SID==''):?>
                    <button class="btn btn-outline-danger" type="button" onclick="fnGoToLogin()"><i class="fas fa-shopping-cart"></i> เพิ่มไปยังรถเข็น</button>
                <?php else:?>
                    <button class="btn btn-outline-danger" type="button" onclick="modalAddProduct()"><i class="fas fa-shopping-cart"></i> เพิ่มไปยังรถเข็น</button>
                <?php endif;?>
            </div>
        </div>
    </div>

    <!-- Gallery -->
    <div class="row shadow p-2 mt-5 bg-white rounded">
        <div class="p-2">
            <h5 class="text-secondary pb-2 border-bottom"><i class="fas fa-image mx-2"></i>อัลบัมภาพ</h5>
        </div>
        <div class="p-2">
            <div class="container-fluid">
                <div class="row row-cols-2 row-cols-md-5 g-3">
                    <?php foreach ($UPLOADS as $item):?>
                    <div class="col card-box-news">
                        <div class="card shadow bg-cake p-2">
                            <img src="<?php echo $item['img'];?>" class="imageZoomIn card-img-top card-gallery-img">
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>

    <!-- slide production-->
    <div class="shadow p-2 mt-5 bg-white">
        <div class="p-2">
            <h5 class="text-secondary pb-2 border-bottom"><i class="fas fa-cookie-bite mx-2"></i> เปี๊ยะ ฟิน ฟิน จร้า ^^</h5>
        </div>
        <div class="table-responsive">
            <table class="table table-borderless text-nowrap">
                <tr>
                    <?php foreach ($PRODUCTS as $key=>$item):?>
                    <td>
                        <div class="hover-shadow card shadow bg-cake" style="max-width: 200px;" onclick="window.location.href='page-view.php?id=<?php echo $item['id'];?>';">
                            <img src="<?php echo $item['img'];?>" class="card-img-top card-gallery-img">
                            <div class="card-body p-1">
                                <small class="card-title text-center"><?php echo $item['product_name'];?></small>
                                <div class="clearfix">
                                    <div class="float-end">
                                        <span class="small badge bg-danger"><?php echo $item['price'];?> -</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <?php endforeach;?>
                </tr>
            </table>
        </div>
    </div>

    <!-- comment -->
    <div class="shadow p-2 mt-5 bg-white">
        <div class="p-2">
            <h5 class="text-secondary border-bottom mb-3 pb-2">
                <i class="fas fa-comments mr-2"></i> Comment
            </h5>
        </div>
        <div class="p-3">
            <?php foreach ($COMMENTS as $item):?>
            <div class="card mb-2 shadow bg-comment" style="max-width: 700px;">
                <div class="row no-gutters align-items-center">
                    <div class="col-md-4 text-center p-1">
                        <img src="img/icon-fm.png" class="card-img rounded-circle w-25">
                        <strong class="d-block text-primary"> <?php echo $item['comment_name'];?></strong>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title text-secondary"><?php echo $item['comment_name'];?></h5>
                            <p class="card-text"> <?php echo $item['comment_post'];?> </p>
                            <p class="card-text">
                                <small class="text-muted"> <?php echo dThai($item['create_at']);?> </small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
        <div class="mt-1 mb-2">
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-sm justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <!-- post -->
    <div class="p-2 mt-5">
        <div class="mb-2">
            <div class="shadow p-3 mx-auto bg-white bg-doc" style="max-width: 700px;">
                <div class="mt-3 text-center">
                    <h5>Post Comment</h5>
                </div>
                <!-- แจ้งเตือน -->
                <?php if($alertStatus):?>
                    <div class="alert <?php echo $alertType=='S'?'alert-success':($alertType=='D'?'alert-danger':'alert-warning');?> alert-dismissible fade show" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        <strong class="alert-heading">แจ้งเตือน</strong>
                        <span class="d-block"><?php echo $alertText;?></span>
                    </div>
                <?php endif;?>

                <form class="mb-5" method="post">
                    <div class="form-group">
                        <label for="inputCommentId">Comment <span class="text-danger">*</span> </label>
                        <textarea id="inputCommentId" class="form-control" name="comment" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="inputNameId">Name <span class="text-danger">*</span> </label>
                        <input id="inputNameId" class="form-control" name="name" type="text" required>
                    </div>
                    <div class="text-center mt-2">
                        <input class="d-none" type="text" name="action" value="comment" placeholder="">
                        <button class="btn btn-outline-info" type="submit"> Post Comment </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<!-- ____________ End: Body ____________ -->


<!-- =========== Start: Footer =========== -->
<div class="container-fluid footer pt-4 pb-4 bg-8">
    <?php require_once __DIR__.'/_footer.php';?>
</div>
<!-- ____________ End: Footer ____________ -->


<!-- =========== Start: Modal =========== -->
<!-- modal go to login -->
<div class="modal fade" id="modalGoToLogin" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalGoToLoginLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalGoToLoginLabel">กรุณาเข้าสู่ระบบ</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="text-center mb-3">
                    <i class="fas fa-exclamation-triangle fa-4x text-warning"></i>
                </div>
                <div class="text-center text-warning">
                    <strong>กรุณาเข้าสู่ระบบ</strong>
                    <p>ไปยังหน้าเข้าสู่ระบบเพื่อทำการสั่งซื้อสินค้า</p>
                </div>
            </div>
            <form class="modal-footer" method="post" action="page-login.php">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success"> เข้าสู่ระบบ </button>
            </form>
        </div>
    </div>
</div>

<!-- modal go to login -->
<div class="modal fade" id="modalAddProduct" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalAddProductLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content" method="post">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAddProductLabel">เพิ่มสินค้า</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="p-2 text-center">
                    <img class="img-fluid" src="<?php echo $PRODUCT['img'];?>">
                </div>
                <div class="row g-3">
                    <div class="col-md-8">
                        <label class="t-r" for="mAPNameId"> สินค้า </label>
                        <input id="mAPNameId" class="form-control" type="text" name="product_name" value="<?php echo $PRODUCT['product_name'];?>" readonly>
                    </div>
                    <div class="col-md-4">
                        <label class="t-r" for="mAPPriceId"> ราคา </label>
                        <input id="mAPPriceId" class="form-control text-danger" type="text" name="product_price" value="<?php echo $PRODUCT['price'];?>" readonly>
                    </div>

                    <div class="col-md-6">
                        <label class="t-r" for="mAPNumberId"> จำนวน </label>
                        <input id="mAPNumberId" class="form-control" min="1" type="number" name="order_number" required>
                    </div>
                    <div class="col-md-6">
                        <label class="t-r" for="mAPSumId"> รวม </label>
                        <input id="mAPSumId" class="form-control text-danger" type="text" name="product_sum" readonly>
                    </div>

                    <div class="col-md-12">
                        <label for="mAPNoteId"> รายละเอียด </label>
                        <textarea id="mAPNoteId" class="form-control" name="order_note"></textarea>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <input class="d-none form-control" name="action" value="addProduct" placeholder="">
                <input class="d-none form-control" name="product_id" value="<?php echo $productId;?>" placeholder="">
                <input class="d-none form-control" name="member_id" value="<?php echo $S_SID;?>" placeholder="">

                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-info"> เพิ่มไปยังตะกร้าสินค้า </button>
            </div>
        </form>
    </div>
</div>

<!-- ____________ End : Modal ____________ -->

<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_script.php';?>
<!-- ____________ End: Script ____________ -->

<!-- modal product shop -->
<?php require_once __DIR__.'/_modalProduct.php';?>

<!-- gallery -->
<div id="modalGallery" class="galleryModal">
    <span class="close" id="closeGallery">&times;</span>
    <img class="galleryModal-content img-fluid rounded-lg" id="imgGallery" src="">
</div>
<script>
    $(".imageZoomIn").on('click',function () {
        let img = $(this).attr("src");
        $("#imgGallery").attr("src",img);
        $("#modalGallery").css("display", "block");
    });
    $("#closeGallery").on('click',function () {
        $("#modalGallery").css("display", "none");
    });
</script>

<!-- =========== Start: Custom Script =========== -->
<script>

    //add reduce
    function fnAdd() {
        let v = $("#inputNumberId").val();
        v = parseInt(v);
        v = v+1;
        $("#inputNumberId").val(v);
    }
    function fnReduce() {
        let v = $("#inputNumberId").val();
        v = parseInt(v);
        v= v-1;
        v = v<=0?1:v;
        $("#inputNumberId").val(v);
    }

    //go to login
    function fnGoToLogin() {
        $("#modalGoToLogin").modal("show");
    }
    //add product
    function modalAddProduct() {
        let num = $("#inputNumberId").val();
        $("#mAPNumberId").val(num);

        fnChangeNum();
        $("#modalAddProduct").modal("show");
    }
    //change number sum product
    function fnChangeNum() {
        let num = $("#mAPNumberId").val();
        let price = $("#mAPPriceId").val();

        num = parseFloat(num);
        price = parseFloat(price);

        let sum = num * price;
        $("#mAPSumId").val(sum);
    }
    $("#mAPNumberId").on("change",function () {
        fnChangeNum();
    });

</script>
<!-- ____________ End: Custom Script ____________ -->


</body>
</html>

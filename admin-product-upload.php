<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 12:31 PM
 */
$MENU = 'product';
require_once __DIR__.'/controller/adminProductUploadController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_adminHeader.php';?>

</head>
<body>

<!-- =========== Start: Top Menu =========== -->
<div class="sticky-top">
    <?php require_once __DIR__.'/_adminMenu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="pl-2 pr-2">
    <div class="container-fluid">
        <div class="page-content shadow mt-3 mb-5 p-3">

            <!-- header -->
            <div class="mb-3 border-bottom pb-1">
                <a class="text-decoration-none me-2" href="admin-product.html">
                    <span class="h5"><i class="fas fa-gifts"></i> สินค้า</span>
                </a>  /
                <span class="text-muted h5 ms-2"> อัลบัมภาพ </span>
            </div>

            <!-- แจ้งเตือน -->
            <?php if($alertStatus):?>
            <div class="alert <?php echo $alertType=='S'?'alert-success':($alertType=='D'?'alert-danger':'alert-warning');?> alert-dismissible fade show" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong class="alert-heading">แจ้งเตือน</strong>
                <span class="d-block"><?php echo $alertText;?></span>
            </div>
            <?php endif;?>

            <!-- detail -->
            <div class="container border-bottom pb-3 mb-3">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <img class="img-fluid img-thumbnail rounded" src="<?php echo $productImg;?>" style="max-height: 200px;max-width: 250px;">
                    </div>
                    <div class="col-md-6">
                        <p>
                            สินค้า :
                            <span class="fw-bold text-primary"><?php echo $productName;?></span>
                        </p>
                        <p>
                            ราคา :
                            <span class="fw-bold t text-danger"> <?php echo number_format($productPrice);?> </span> บาท
                        </p>
                        <span class="d-block"> รายละเอียด : </span>
                        <span class="text-muted"><?php echo $productDetail;?></span>
                    </div>
                </div>
            </div>

            <!-- upload image and gallery -->
            <div class="container mb-3">
                <!-- upload image -->
                <div class="mb-5">
                    <div class="mt-1">
                        <div class="text-center mt-2">
                            <label class="p-2 text-center" for="imageUploadId" style="cursor: pointer;">
                                <img id="pathImageUploadId" class="img-fluid" src="img/df-upload.png" style="max-height: 300px;max-width: 350px;border:2px dashed #0d6efd;">
                                <input class="d-none" id="imageUploadId" accept="image/*" type="file" onchange="imageUpload(this)">
                            </label>
                        </div>
                        <div class="text-center small text-muted">
                            อัพโหลดภาพ ( 500 x 500 px )
                        </div>
                        <div class="row align-items-center d-none" id="imageUploadProgressbarId">
                            <div class="col">
                                <div class="progress">
                                    <div id="progressbarId" class="progress-bar" style="width: 1%;" role="progressbar"></div>
                                </div>
                            </div>
                            <div class="col-auto pl-0">
                                <span id="cancelUploadImageId" class="badge bg-danger" style="cursor: pointer;" onclick="cancelUpload()"><i class="fas fa-times"></i></span>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- gallery -->
                <div class="mb-3">
                    <div class="p-2">
                        <h5 class="text-secondary pb-2 border-bottom"><i class="fas fa-images mx-2"></i>อัลบัมภาพ</h5>
                    </div>
                    <div class="p-2">
                        <div class="container-fluid">
                            <div class="row row-cols-2 row-cols-md-3 g-3">

                                <?php foreach ($UPLOADS as $key=>$item):?>
                                <div class="col card-box-news">
                                    <div class="card shadow bg-cake p-2">
                                        <img src="<?php echo $item['img'];?>" class="imageZoomIn card-img-top card-gallery-img">
                                        <div class="card-footer text-center">
                                            <button class="btn btn-outline-danger btn-sm" type="button" onclick="modalDeleteShow()">
                                                <i class="far fa-trash-alt"></i> ลบ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach;?>

                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>

<!-- modal delete -->
<div class="modal fade" id="modalDelete" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-danger" id="modalDeleteLabel">ลบข้อมูล</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="text-center mb-3">
                    <i class="fas fa-ban fa-4x text-danger"></i>
                </div>
                <div class="text-center text-danger">
                    <strong>Delete</strong>
                    <p>ลบข้อมูล</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" onclick="modalDeleteHide()">Delete</button>
            </div>
        </div>
    </div>
</div>


<!-- modal save img -->
<div class="modal fade" id="modalImg" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalImgLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-success" id="modalImgLabel">Image</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <img id="mImgIdSrc" class="img-fluid" src="img/df-upload.png">
            </div>
            <form class="modal-footer" method="post">
                <input class="d-none" type="text" name="action" value="uploadImg" placeholder="">
                <input id="mImgIdPath" class="d-none" type="text" name="path" value="" placeholder="">
                <button type="submit" class="btn btn-success">บันทึก</button>
            </form>
        </div>
    </div>
</div>


<!-- ____________ End: Body ____________ -->


<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_adminScript.php';?>
<!-- ____________ End: Script ____________ -->


<!-- =========== Start: Custom Script =========== -->
<script>

    //upload image
    let ajax_img;
    function imageUpload(input) {
        if (input.files && input.files[0]) {
            ajax_img = new XMLHttpRequest();

            $('#imageUploadProgressbarId').removeClass('d-none');

            let form_data = new FormData();
            form_data.append("fileToUpload", input.files[0]);
            ajax_img.upload.addEventListener("progress", progressHandler, false);
            ajax_img.addEventListener("load", completeHandler, false);
            ajax_img.addEventListener("error", errorHandler, false);
            ajax_img.addEventListener("abort", abortHandler, false);
            ajax_img.open("POST","/upload/upload_file.php");
            ajax_img.send(form_data);

            function progressHandler(event) {
                let percent = (event.loaded / event.total) * 100;
                $("#progressbarId").css('width', Math.round(percent) + "%");
            }

            function completeHandler(event) {
                let data_return = JSON.parse(event.target.responseText);
                if (data_return['status'] === 'ok') {
                    let src = '/upload/uploads/'+data_return['new_name'];

                    $("#progressbarId").css('width', "0%");
                    $("#imageUploadProgressbarId").addClass("d-none");

                    $("#mImgIdSrc").attr("src",src);
                    $("#mImgIdPath").val(src);
                    $("#modalImg").modal("show");

                } else {
                    ajax_img.abort();
                    $("#progressbarId").css('width', "0%");
                    $("#imageUploadProgressbarId").addClass("d-none");
                    alert("Error:" + data_return['message']);
                }
            }

            function errorHandler(event) {
                ajax_img.abort();
                $("#progressbarId").css('width', "0%");
                $("#imageUploadProgressbarId").addClass("d-none");
                alert("Upload Failed");
            }

            function abortHandler(event) {
                ajax_img.abort();
                $("#progressbarId").css('width', "0%");
                $("#imageUploadProgressbarId").addClass("d-none");
                alert("Upload Aborted");
            }
        }
        else {
            alert("Not found file input!!!");
        }
    }
    function cancelUpload() {
        if(ajax_img!=null){
            ajax_img.abort();
        }
        $("#progressbarId").css('width', "0%");
        $("#imageUploadProgressbarId").addClass("d-none");
        alert("Cancel Upload Image");
    }

    //delete
    function modalDeleteShow() {
        $("#modalDelete").modal("show");
    }
    function modalDeleteHide() {
        $("#modalDelete").modal("hide");
    }

</script>
<!-- ____________ End: Custom Script ____________ -->


</body>

</html>

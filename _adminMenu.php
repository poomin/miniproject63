<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 12:38 PM
 */
$MENU = isset($MENU)?$MENU:'';
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="img/logo-page.jpg" style="width: 30px;height: 30px;" class="d-inline-block align-top rounded" alt="">
            Logo
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link <?php echo $MENU=='home'?'fw-bold text-primary':'text-muted';?>" href="admin-home.php">
                        <i class="fas fa-home"></i> Home
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo $MENU=='shop'?'fw-bold text-primary':'text-muted';?>" href="admin-shop.php">
                        <i class="fas fa-store"></i> ร้านค้า
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo $MENU=='product'?'fw-bold text-primary':'text-muted';?>" href="admin-product.php">
                        <i class="fas fa-gifts"></i> สินค้า
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo $MENU=='member'?'fw-bold text-primary':'text-muted';?>" href="admin-member.php">
                        <i class="fas fa-users"></i> สมาชิก
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo $MENU=='bank'?'fw-bold text-primary':'text-muted';?>" href="admin-bank.php">
                        <i class="fas fa-university"></i> ธนาคาร
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo $MENU=='sell'?'fw-bold text-primary':'text-muted';?>" href="admin-sell.php">
                        <i class="fas fa-cart-plus"></i> สั่งซื้อ <span class="badge bg-danger rounded-pill">10</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-muted" href="_logout.php">
                        <i class="fas fa-sign-in-alt"></i> ออก
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

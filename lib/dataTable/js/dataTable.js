$(document).ready(function() {
    $(".dataTable").DataTable();

    $(".dataTableNone").dataTable({
        "bInfo" : false
    });

    $(".dataTableNone50").dataTable({
        "bInfo" : false,
        "pageLength": 50
    });

} );
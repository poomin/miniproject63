<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/18/2021
 * Time: 2:16 PM
 */



session_start();
unset($_SESSION['SId']);
unset($_SESSION['SName']);
unset($_SESSION['SType']);
unset($_SESSION['SCount']);
header( "location: /page-index.php" );
exit(0);
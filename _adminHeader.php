<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 12:34 PM
 */
?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="img/logo-page.ico">
<title>Admin</title>

<!-- bootstrap css -->
<link rel="stylesheet" href="bootstrap-5.0.0/css/bootstrap.min.css">

<!-- font awesome -->
<link rel="stylesheet" href="fontawesome-5.15.1/css/all.min.css">

<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 12:31 PM
 */
$MENU = 'home';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_adminHeader.php';?>

</head>
<body>

<!-- =========== Start: Top Menu =========== -->
<div class="sticky-top">
    <?php require_once __DIR__.'/_adminMenu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="pl-2 pr-2">
    <div class="container-fluid">
        <div class="page-content shadow mt-3 mb-5 p-3">

            <!-- header -->
            <div class="mb-3 border-bottom pb-1">
                <h5> <i class="fas fa-home"></i> หน้าหลัก</h5>
            </div>

            <!-- graph -->
            <div class="p-5 mb-5">
                <div id="lineChartId" style="width: 100%; height: 500px"></div>
            </div>

        </div>
    </div>

</div>
<!-- ____________ End: Body ____________ -->


<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_adminScript.php';?>
<!-- ____________ End: Script ____________ -->


<!-- =========== Start: Custom Script =========== -->
<!-- script graph -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>

    //google graph
    google.charts.load('current', {'packages':['line']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {

        let data = new google.visualization.DataTable();
        data.addColumn('string', 'ว/ด/ป');
        data.addColumn('number', 'ยอดขาย');

        data.addRows([
            ['1/04/64', 590],
            ['2/04/64', 245],
            ['3/04/64', 0],
            ['4/04/64', 1050],
            ['5/04/64', 460],
            ['6/04/64', 680],
            ['7/04/64', 350],
            ['8/04/64', 850],
            ['9/04/64', 720],
            ['10/04/64', 950]
        ]);

        let options = {
            chart: {
                title: 'กราฟแสดงยอดขายรายวัน',
                subtitle: 'บาท'
            }
        };

        let chart = new google.charts.Line(document.getElementById('lineChartId'));

        chart.draw(data, google.charts.Line.convertOptions(options));
    }

</script>
<!-- ____________ End: Custom Script ____________ -->


</body>

</html>

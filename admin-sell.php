<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/9/2021
 * Time: 12:31 PM
 */
$MENU = 'sell';
require_once __DIR__.'/controller/adminSellController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Header -->
    <?php require_once __DIR__.'/_adminHeader.php';?>

    <!-- dataTable bootstrap 5 -->
    <link rel="stylesheet" href="lib/dataTable/css/dataTables.bootstrap5.min.css">

    <style>
        /* gallery modal */
        .galleryModal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1100; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
        }
        /* gallery modal content */
        .galleryModal-content {
            margin: auto;
            display: block;
            width: auto;
            height: auto;
            max-height: 90%;
            max-width: 90%;

            animation-name: zoom;
            animation-duration: 0.6s;
        }
        @keyframes zoom {
            from {transform:scale(0)}
            to {transform:scale(1)}
        }
        /* gallery modal Close Button */
        #closeGallery {
            position: absolute;
            top: 20px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }
        #closeGallery:hover,
        #closeGallery:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }
        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px){
            .galleryModal-content {
                width: 96%;
            }
        }

    </style>

</head>
<body>

<!-- =========== Start: Top Menu =========== -->
<div class="sticky-top">
    <?php require_once __DIR__.'/_adminMenu.php';?>
</div>
<!-- ____________ End: Top Menu ____________ -->


<!-- =========== Start: Body =========== -->
<div class="pl-2 pr-2">
    <div class="container-fluid">
        <div class="page-content shadow mt-3 mb-5 p-3">

            <!-- header -->
            <div class="mb-3 border-bottom pb-1">
                <h5 class="float-left"><i class="fas fa-cart-plus"></i> สั่งซื้อ</h5>
            </div>


            <!-- alert message -->
            <?php if($thisAlertStatus=='S'):?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <strong class="alert-heading">สำเร็จ</strong>
                    <span class="d-block"><?php echo $thisAlertText;?></span>
                </div>
            <?php elseif ($thisAlertStatus=='E'):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <strong class="alert-heading">แจ้งเตือน</strong>
                    <span class="d-block"><?php echo $thisAlertText;?></span>
                </div>
            <?php endif;?>

            <div class="mb-5">
                <ul class="nav nav-tabs fw-bold nav-fill" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" id="p1-tab" data-bs-toggle="tab" data-bs-target="#p1Id" role="tab" aria-controls="p1" aria-selected="true">
                            <span class="fw-bold text-info"> ตรวจสอบการสั่งซื้อ </span>
                            <?php if($countWait>0):?>
                            <span class="badge bg-danger ms-2"><?php echo $countWait;?></span>
                            <?php endif;?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="p2-tab" data-bs-toggle="tab" data-bs-target="#p2Id" role="tab" aria-controls="p2" aria-selected="false">
                            <span class="fw-bold text-success"> รอจัดส่ง </span>
                            <?php if($countSend>0):?>
                            <span class="badge bg-danger ms-2"><?php echo $countSend;?></span>
                            <?php endif;?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="p3-tab" data-bs-toggle="tab" data-bs-target="#p3Id" role="tab" aria-controls="p3" aria-selected="false">
                            <span class="fw-bold text-danger"> ประวัติ </span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="p-2 tab-pane fade show active" id="p1Id" role="tabpanel" aria-labelledby="p1-tab">

                        <div class="mt-4 border-bottom">
                            <h5 class="text-dark"><i class="fas fa-angle-double-right"></i> ตรวจสอบการสั่งซื้อ</h5>
                            <p class="text-muted">ทำการตรวจสอบการชำระเงิน ที่อยู่ สินค่า วิธีการรับส่ง และสลิปการชำระเงิน ก่อนยืนยัน</p>
                        </div>
                        <div class="mt-3">
                            <div class="table-responsive mb-5">
                                <table class="dataTableNone50 table table-sm text-nowrap w-100">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Id</th>
                                        <th>วันที่</th>
                                        <th>ลูกค้า</th>
                                        <th>รายละเอียด</th>
                                        <th>slip</th>
                                        <th>ยอด</th>
                                        <th>จัดการ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($billWait as $key=>$item):?>
                                    <tr>
                                        <td><?php echo ($key+1);?></td>
                                        <td><?php echo $item['id'];?></td>
                                        <td><?php echo $item['bill_date'];?></td>
                                        <td><?php echo $item['member_name'];?></td>
                                        <td>
                                            <div>
                                                ค่าสินค้า : <span class="badge bg-danger"><?php echo number_format($item['bill_price'],2,'.',',');?> </span>
                                            </div>
                                            <div>
                                                ค่าจัดส่ง : <span class="badge bg-danger"><?php echo number_format($item['shipping_price'],2,'.',',');?> </span>
                                                <div class="text-muted" style="font-size: 14px;"><?php echo $item['shipping_name'];?></div>
                                            </div>
                                            <div>
                                                ผู้รับ : <strong class="text-dark me-2"><?php echo $item['member_name'];?></strong>
                                                Tel : <strong class="text-dark"><?php echo $item['member_tel'];?></strong>
                                            </div>
                                            <div>
                                                ที่อยู่ : <span class="text-dark"><?php echo $item['member_address'];?></span>
                                            </div>
                                        </td>
                                        <td>
                                            <button class="btn btn-outline-success btn-sm" type="button" onclick="showImg('<?php echo $item['slip_img'];?>')"><i class="fas fa-image"></i></button>
                                        </td>
                                        <td class="fw-bold text-center">
                                            <?php echo number_format(($item['bill_price']+$item['shipping_price']),2,'.',',');?>
                                        </td>
                                        <td>
                                            <button class="btn btn-outline-info btn-sm" type="button" onclick="modalSellShow(<?php echo $item['id'];?>)">
                                                <i class="far fa-edit"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="p2Id" role="tabpanel" aria-labelledby="p2-tab">

                        <div class="mt-4 border-bottom">
                            <h5 class="text-dark"><i class="fas fa-angle-double-right"></i> รายการจัดส่ง</h5>
                            <p class="text-muted">ทำการจัดส่งสินค้า และรับหมายเลขรับส่งสินค้า</p>
                        </div>
                        <div class="mt-3">
                            <div class="table-responsive mb-5">
                                <table class="dataTableNone50 table table-sm text-nowrap w-100">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Id</th>
                                        <th>วันที่</th>
                                        <th>ลูกค้า</th>
                                        <th>รายละเอียด</th>
                                        <th>ยอด</th>
                                        <th>จัดการ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($billSend as $key=>$item):?>
                                        <tr>
                                            <td><?php echo ($key+1);?></td>
                                            <td><?php echo $item['id'];?></td>
                                            <td><?php echo $item['bill_date'];?></td>
                                            <td><?php echo $item['member_name'];?></td>
                                            <td>
                                                <div>
                                                    ค่าสินค้า : <span class="badge bg-danger"><?php echo number_format($item['bill_price'],2,'.',',');?> </span>
                                                </div>
                                                <div>
                                                    ค่าจัดส่ง : <span class="badge bg-danger"><?php echo number_format($item['shipping_price'],2,'.',',');?> </span>
                                                    <div class="text-muted" style="font-size: 14px;"><?php echo $item['shipping_name'];?></div>
                                                </div>
                                                <div>
                                                    ผู้รับ : <strong class="text-dark me-2"><?php echo $item['member_name'];?></strong>
                                                    Tel : <strong class="text-dark"><?php echo $item['member_tel'];?></strong>
                                                </div>
                                                <div>
                                                    ที่อยู่ : <span class="text-dark"><?php echo $item['member_address'];?></span>
                                                </div>
                                            </td>
                                            <td class="fw-bold text-center">
                                                <?php echo number_format(($item['bill_price']+$item['shipping_price']),2,'.',',');?>
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-info btn-sm" type="button" onclick="modalShipShow(<?php echo $item['id'];?>)">
                                                    <i class="far fa-edit"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="p3Id" role="tabpanel" aria-labelledby="p3-tab">

                        <div class="mt-4 border-bottom">
                            <h5 class="text-dark"><i class="fas fa-angle-double-right"></i> ประวัติรายการ </h5>
                            <p class="text-muted">รายการจัดส่งสินค้า</p>
                        </div>
                        <div class="mt-3">
                            <div class="table-responsive mb-5">
                                <table class="dataTableNone50 table table-sm text-nowrap w-100">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Id</th>
                                        <th>วันที่</th>
                                        <th>ลูกค้า</th>
                                        <th>สถานะ</th>
                                        <th>ยอด</th>
                                        <th>จัดการ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($billAll as $key=>$item):?>
                                        <tr>
                                            <td><?php echo ($key+1);?></td>
                                            <td><?php echo $item['id'];?></td>
                                            <td><?php echo $item['bill_date'];?></td>
                                            <td><?php echo $item['member_name'];?></td>
                                            <td>
                                                <?php if($item['bill_type']=='B'):?>
                                                    <span class="badge bg-warning">รอตรวจสอบ</span>
                                                <?php elseif ($item['bill_type']=='P'):?>
                                                    <span class="badge bg-success">รอจัดส่ง</span>
                                                <?php elseif ($item['bill_type']=='C'):?>
                                                    <span class="badge bg-danger">ยกเลิกรายการ</span>
                                                <?php elseif ($item['bill_type']=='S'):?>
                                                    <span class="badge bg-info">รอลูกค้ารับสินค้า</span>
                                                <?php elseif ($item['bill_type']=='E'):?>
                                                    <span class="badge bg-info">เสร็จสิ้น</span>
                                                <?php endif;?>
                                            </td>
                                            <td class="fw-bold text-center">
                                                <?php echo number_format(($item['bill_price']+$item['shipping_price']),2,'.',',');?>
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-info btn-sm" type="button" onclick="modalViewShow(<?php echo $item['id'];?>)">
                                                    <i class="far fa-eye"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>

</div>
<!-- ____________ End: Body ____________ -->

<!-- =========== Start: Modal =========== -->
<!-- modal Sell -->
<div class="modal fade" id="modalSell" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalSellLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form class="modal-content" method="post">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSellLabel">ยืนยันรายการ</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <div class="row mb-3">
                    <div class="col-md-8">
                        <div class="mb-3">

                            <div class="mb-1">
                                OrderId : <strong id="mSId" class="fw-bold text-muted me-5"></strong>
                                วันที่ : <strong id="mSDate" class="fw-bold text-muted me-5"></strong>
                            </div>
                            <div class="mb-1">
                                ลูกค้า: <strong id="mSName" class="fw-bold text-primary me-5"></strong>
                                เบอร์โทร: <strong id="mSTel" class="fw-bold text-primary"></strong>
                            </div>

                            <div class="mb-1">
                                ที่อยู่: <span id="mSAddress" class="fw-bold text-primary"></span>
                            </div>
                            <div class="mb-1">
                                วิธีการจัดส่ง <span id="mSShipName" class="fw-bold text-primary me-5"></span>
                                ราคาจัดส่ง <span id="mSShipPrice" class="fw-bold text-danger"></span> บาท
                            </div>
                            <div class="mb-1">
                                ราคาสินค้า <span id="mSPrice" class="fw-bold text-danger ms-2 me-1"></span> บาท
                            </div>
                            <div class="mb-3">
                                ยอดชำระ <span id="mSTotal" class="fw-bold text-danger ms-2 me-1"> </span> บาท
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <img id="mSImg" class="img-fluid img-thumbnail" src="img/df-upload.png" style="max-height: 150px;">
                        <span class="d-block text-muted">สลิป</span>
                    </div>
                </div>

                <div class="container">
                    <div class="table-responsive mb-5">
                        <table class="table table-bordered table-sm text-nowrap w-100">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>รายการ</th>
                                <th>เพิ่มเติม</th>
                                <th>จำนวน</th>
                                <th>ราคา</th>
                                <th>รวม</th>
                            </tr>
                            </thead>
                            <tbody id="mSTable"></tbody>
                        </table>
                    </div>
                </div>

                <div class="mt-1">
                    <span>สถานะ</span>
                    <div class="mb-2">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="bill_type" id="mSBillTypeP" value="P" checked>
                            <label class="form-check-label text-success" for="mSBillTypeP">ตรวจสอบรายการผ่านแล้ว</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="bill_type" id="mSBillTypeC" value="C">
                            <label class="form-check-label text-danger" for="mSBillTypeC">ตรวจสอบรายการไม่ผ่าน</label>
                        </div>
                    </div>
                    <div class="mb-2">
                        <label for="mSBillNoteId">รายละเอียดเพิ่มเติม</label>
                        <input id="mSBillNoteId" name="bill_note" class="form-control" type="text" value="">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input class="d-none" type="text" name="action" value="confirm" placeholder="">
                <input id="mSBillId" class="d-none" type="text" name="bill_id" value="" placeholder="">

                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">ยืนยันรายการ</button>
            </div>
        </form>
    </div>
</div>
<!-- modal Sell Ship -->
<div class="modal fade" id="modalShip" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalShipLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form class="modal-content" method="post">
            <div class="modal-header">
                <h5 class="modal-title" id="modalShipLabel">รายการจัดส่งสินค้า</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <div class="row mb-3">
                    <div class="col-md-8">
                        <div class="mb-3">

                            <div class="mb-1">
                                OrderId : <strong id="mCId" class="fw-bold text-muted me-5"></strong>
                                วันที่ : <strong id="mCDate" class="fw-bold text-muted me-5"></strong>
                            </div>
                            <div class="mb-1">
                                ลูกค้า: <strong id="mCName" class="fw-bold text-primary me-5"></strong>
                                เบอร์โทร: <strong id="mCTel" class="fw-bold text-primary"></strong>
                            </div>

                            <div class="mb-1">
                                ที่อยู่: <span id="mCAddress" class="fw-bold text-primary"></span>
                            </div>
                            <div class="mb-1">
                                วิธีการจัดส่ง <span id="mCShipName" class="fw-bold text-primary me-5"></span>
                                ราคาจัดส่ง <span id="mCShipPrice" class="fw-bold text-danger"></span> บาท
                            </div>
                            <div class="mb-1">
                                ราคาสินค้า <span id="mCPrice" class="fw-bold text-danger ms-2 me-1"></span> บาท
                            </div>
                            <div class="mb-3">
                                ยอดชำระ <span id="mCTotal" class="fw-bold text-danger ms-2 me-1"> </span> บาท
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <img id="mCImg" class="img-fluid img-thumbnail" src="img/df-upload.png" style="max-height: 150px;">
                        <span class="d-block text-muted">สลิป</span>
                    </div>
                </div>

                <div class="container">
                    <div class="table-responsive mb-5">
                        <table class="table table-bordered table-sm text-nowrap w-100">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>รายการ</th>
                                <th>เพิ่มเติม</th>
                                <th>จำนวน</th>
                                <th>ราคา</th>
                                <th>รวม</th>
                            </tr>
                            </thead>
                            <tbody id="mCTable"></tbody>
                        </table>
                    </div>
                </div>

                <div class="mt-1">
                    <div class="mb-2">
                        <label for="mCBillNoteId">หมายเลขการส่งสินค้า <span class="text-danger">!</span></label>
                        <input id="mCBillNoteId" name="shipping_code" class="form-control" type="text" value="" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input class="d-none" type="text" name="action" value="ship" placeholder="">
                <input id="mCBillId" class="d-none" type="text" name="bill_id" value="" placeholder="">

                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">ยืนยันรายการ</button>
            </div>
        </form>
    </div>
</div>
<!-- modal Sell View -->
<div class="modal fade" id="modalView" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalViewLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalViewLabel">รายการสินค้า</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <div class="row mb-3">
                    <div class="col-md-8">
                        <div class="mb-3">

                            <div class="mb-1">
                                OrderId : <strong id="mVId" class="fw-bold text-muted me-5"></strong>
                                วันที่ : <strong id="mVDate" class="fw-bold text-muted me-5"></strong>
                            </div>
                            <div class="mb-1">
                                ลูกค้า: <strong id="mVName" class="fw-bold text-primary me-5"></strong>
                                เบอร์โทร: <strong id="mVTel" class="fw-bold text-primary"></strong>
                            </div>

                            <div class="mb-1">
                                ที่อยู่: <span id="mVAddress" class="fw-bold text-primary"></span>
                            </div>
                            <div class="mb-1">
                                วิธีการจัดส่ง <span id="mVShipName" class="fw-bold text-primary me-5"></span>
                                ราคาจัดส่ง <span id="mVShipPrice" class="fw-bold text-danger"></span> บาท
                            </div>
                            <div class="mb-1">
                                ราคาสินค้า <span id="mVPrice" class="fw-bold text-danger ms-2 me-1"></span> บาท
                            </div>
                            <div class="mb-3">
                                ยอดชำระ <span id="mVTotal" class="fw-bold text-danger ms-2 me-1"> </span> บาท
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <img id="mVImg" class="img-fluid img-thumbnail" src="img/df-upload.png" style="max-height: 150px;">
                        <span class="d-block text-muted">สลิป</span>
                    </div>
                </div>

                <div class="container">
                    <div class="table-responsive mb-5">
                        <table class="table table-bordered table-sm text-nowrap w-100">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>รายการ</th>
                                <th>เพิ่มเติม</th>
                                <th>จำนวน</th>
                                <th>ราคา</th>
                                <th>รวม</th>
                            </tr>
                            </thead>
                            <tbody id="mVTable"></tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- ____________ End: Modal ____________ -->

<!-- =========== Start: Script =========== -->
<?php require_once __DIR__.'/_adminScript.php';?>
<!-- ____________ End: Script ____________ -->


<!-- css dataTable bootstrap 5-->
<script src="lib/dataTable/js/jquery.dataTables.min.js"></script>
<script src="lib/dataTable/js/dataTables.bootstrap5.min.js"></script>
<script src="lib/dataTable/js/dataTable.js"></script>

<!-- gallery -->
<div id="modalGallery" class="galleryModal">
    <span class="close" id="closeGallery">&times;</span>
    <img class="galleryModal-content img-fluid rounded-lg" id="imgGallery" src="">
</div>
<script>
    function showImg(img){
        $("#imgGallery").attr("src",img);
        $("#modalGallery").css("display", "block");
    }
    $("#closeGallery").on('click',function () {
        $("#modalGallery").css("display", "none");
    });
</script>


<!-- =========== Start: Custom Script =========== -->

<script>

    function modalSellShow(id) {
        let req = $.ajax({
            type: 'POST',
            url: '/api/postBillId.php',
            data: {
                id: id,
            },
            dataType: 'JSON'
        });
        req.done(function (res) {
            if(res.code === 200 ){
                //success
                console.log(res);
                let data = res.result;
                let bill = data.bill;
                let list = data.list;

                let shipPrice = bill.shipping_price;
                let proPrice = bill.bill_price;
                let sum = shipPrice + proPrice;

                $("#mSBillId").val(bill.id);
                $("#mSId").html(bill.id);
                $("#mSDate").html(bill.bill_date);

                $("#mSName").html(bill.member_name);
                $("#mSTel").html(bill.member_tel);
                $("#mSAddress").html(bill.member_address);

                $("#mSShipName").html(bill.shipping_name);
                $("#mSShipPrice").html(shipPrice);

                $("#mSImg").attr("src",bill.slip_img);

                $("#mSPrice").html(proPrice);
                $("#mSTotal").html(sum);

                let textTable = "";
                let str = "";
                for(let i=0;i<list.length;i++){
                    str = "<tr>";
                    str += "<td>"+(i+1)+"</td>";
                    str += "<td>"+list[i].product_name+"</td>";
                    str += "<td>"+list[i].order_note+"</td>";
                    str += "<td>"+list[i].order_number+"</td>";
                    str += "<td>"+list[i].product_price+"</td>";
                    str += "<td>"+(list[i].order_number*list[i].product_price)+"</td>";
                    str += "</tr>";
                    textTable+=str;
                }
                $("#mSTable").html(textTable);


                $("#modalSell").modal("show");
            }else{
                //warning
                alert("Warn : "+res.message);
            }
        }).fail(function (e,text){
            //in page show error
            alert("Error : "+text)
        });
    }

    function modalShipShow(id) {
        let req = $.ajax({
            type: 'POST',
            url: '/api/postBillId.php',
            data: {
                id: id,
            },
            dataType: 'JSON'
        });
        req.done(function (res) {
            if(res.code === 200 ){
                //success
                console.log(res);
                let data = res.result;
                let bill = data.bill;
                let list = data.list;

                let shipPrice = bill.shipping_price;
                let proPrice = bill.bill_price;
                let sum = shipPrice + proPrice;

                $("#mCBillId").val(bill.id);
                $("#mCId").html(bill.id);
                $("#mCDate").html(bill.bill_date);

                $("#mCName").html(bill.member_name);
                $("#mCTel").html(bill.member_tel);
                $("#mCAddress").html(bill.member_address);

                $("#mCShipName").html(bill.shipping_name);
                $("#mCShipPrice").html(shipPrice);

                $("#mCImg").attr("src",bill.slip_img);

                $("#mCPrice").html(proPrice);
                $("#mCTotal").html(sum);

                let textTable = "";
                let str = "";
                for(let i=0;i<list.length;i++){
                    str = "<tr>";
                    str += "<td>"+(i+1)+"</td>";
                    str += "<td>"+list[i].product_name+"</td>";
                    str += "<td>"+list[i].order_note+"</td>";
                    str += "<td>"+list[i].order_number+"</td>";
                    str += "<td>"+list[i].product_price+"</td>";
                    str += "<td>"+(list[i].order_number*list[i].product_price)+"</td>";
                    str += "</tr>";
                    textTable+=str;
                }
                $("#mCTable").html(textTable);


                $("#modalShip").modal("show");
            }else{
                //warning
                alert("Warn : "+res.message);
            }
        }).fail(function (e,text){
            //in page show error
            alert("Error : "+text)
        });
    }

    function modalViewShow(id) {
        let req = $.ajax({
            type: 'POST',
            url: '/api/postBillId.php',
            data: {
                id: id,
            },
            dataType: 'JSON'
        });
        req.done(function (res) {
            if(res.code === 200 ){
                //success
                console.log(res);
                let data = res.result;
                let bill = data.bill;
                let list = data.list;

                let shipPrice = bill.shipping_price;
                let proPrice = bill.bill_price;
                let sum = shipPrice + proPrice;

                $("#mVBillId").val(bill.id);
                $("#mVId").html(bill.id);
                $("#mVDate").html(bill.bill_date);

                $("#mVName").html(bill.member_name);
                $("#mVTel").html(bill.member_tel);
                $("#mVAddress").html(bill.member_address);

                $("#mVShipName").html(bill.shipping_name);
                $("#mVShipPrice").html(shipPrice);

                $("#mVImg").attr("src",bill.slip_img);

                $("#mVPrice").html(proPrice);
                $("#mVTotal").html(sum);

                let textTable = "";
                let str = "";
                for(let i=0;i<list.length;i++){
                    str = "<tr>";
                    str += "<td>"+(i+1)+"</td>";
                    str += "<td>"+list[i].product_name+"</td>";
                    str += "<td>"+list[i].order_note+"</td>";
                    str += "<td>"+list[i].order_number+"</td>";
                    str += "<td>"+list[i].product_price+"</td>";
                    str += "<td>"+(list[i].order_number*list[i].product_price)+"</td>";
                    str += "</tr>";
                    textTable+=str;
                }
                $("#mVTable").html(textTable);


                $("#modalView").modal("show");
            }else{
                //warning
                alert("Warn : "+res.message);
            }
        }).fail(function (e,text){
            //in page show error
            alert("Error : "+text)
        });
    }

</script>

<!-- ____________ End: Custom Script ____________ -->


</body>

</html>
